#!/bin/bash

ALL=false
DEBUG=false
RELEASE=false
PROFILE=false

if [ $# == 0 ]
then
	ALL=true
else
	if [[ $1 == "d" ]]
	then
		DEBUG=true
	elif [[ $1 == "r" ]]
	then
		RELEASE=true
	elif [[ $1 == "p" ]]
	then
		PROFILE=true
	else
		echo "Unknown option "$1
	fi
fi

BIN_NAME=deferred_rendering#00

declare -a SOURCES
SOURCES[${#SOURCES[@]}]=`find . -name *.cpp`
# SOURCES[${#SOURCES[@]}]=src/*.cpp
# SOURCES[${#SOURCES[@]}]=src/tgmath*.cpp

declare -a INCLUDE_DIRS
# INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="src/"
# INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="src/tgmath/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="libraries/SDL2/linux/include/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="libraries/SDL2_ttf/linux/include/"
INCLUDE_DIRS[${#INCLUDE_DIRS[@]}]="libraries/SDL2_mixer/linux/include/"

declare -a INCLUDE_QUOTE_DIRS
## NOTE: for SDL2_ttf
INCLUDE_QUOTE_DIRS[${#INCLUDE_QUOTE_DIRS[@]}]="libraries/SDL2/linux/include/SDL2/"

declare -a LIBS_DIRS
LIBS_DIRS[${#LIBS_DIRS[@]}]="libraries/SDL2/linux/lib64/"
LIBS_DIRS[${#LIBS_DIRS[@]}]="libraries/SDL2_ttf/linux/lib64/"
LIBS_DIRS[${#LIBS_DIRS[@]}]="libraries/SDL2_mixer/linux/lib64/"

declare -a LIBS_DIRS
LIBS[${#LIBS[@]}]=SDL2
LIBS[${#LIBS[@]}]=SDL2_ttf
LIBS[${#LIBS[@]}]=SDL2_mixer
LIBS[${#LIBS[@]}]=GL
LIBS[${#LIBS[@]}]=GLEW
LIBS[${#LIBS[@]}]=GLU

function \
include_quote_dirs()
{
	for p in ${INCLUDE_QUOTE_DIRS[@]}
	do
		printf " -iquote$p "
	done
}
function \
include_dirs()
{
	for p in ${INCLUDE_DIRS[@]}
	do
		printf " -I$p "
	done
}
function \
libs_dirs()
{
	for p in ${LIBS_DIRS[@]}
	do
		printf " -L$p -Wl,-rpath=$p "
	done
}
function \
link_libs()
{
	for l in ${LIBS[@]}
	do
		printf " -l$l "
	done
}


include_quote_dirs
echo
include_dirs
echo
libs_dirs
echo
link_libs
echo


if $ALL || $DEBUG
then
	#  -Wno-write-strings
	g++ -fdiagnostics-color=auto -std=c++11 -Wall -O0 -ggdb3 -DDEBUG=1 \
	 `include_quote_dirs` `include_dirs` `libs_dirs` `link_libs` \
	 $SOURCES -o $BIN_NAME"_d"

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "Debug compilation failed! Terminating build script."
		exit $ret
	else
		echo "Debug compilation succeeded!"
	fi
fi

if $ALL || $RELEASE
then
	g++ -fdiagnostics-color=auto -std=c++11 -fno-exceptions -Wall -O3 -DDEBUG=0 -DNDEBUG \
	 `include_quote_dirs` `include_dirs` `libs_dirs` `link_libs` \
	 $SOURCES -o $BIN_NAME

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "Release compilation failed! Terminating build script."
		exit $ret
	else
		echo "Release compilation succeeded!"
	fi
fi

if $ALL || $PROFILE
then
	g++ -pg -fdiagnostics-color=auto -std=c++11 -Wall -O0 -ggdb3 -DDEBUG=1 -DNDEBUG \
	 `include_quote_dirs` `include_dirs` `libs_dirs` `link_libs` \
	 $SOURCES -o $BIN_NAME"_gprof"

	ret=$?
	if [[ $ret != 0 ]]
	then
		echo "Profiling compilation failed! Terminating build script."
		exit $ret
	else
		echo "Profiling compilation succeeded!"
	fi
fi
