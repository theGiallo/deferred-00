#include <GL/glew.h>
#include <GL/gl.h>

#ifdef ANDROID
	#include <SDL.h>
	#include <SDL_video.h>
#else
	#include <SDL2/SDL.h>
	#include <SDL2/SDL_video.h>
#endif
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include <iostream>
#include <utility> // for std::move
#include <sstream>
#include <cassert>

#include <iomanip>

#define APP_NAME "deferred_renderer#00"
#include "dbg.h"

#include "tgmath/MathConsts.h"
#include "tgmath/MathTools.h"
#include "tgmath/Vec2.h"
#include "environment.h"
#include "enginegears.h"
#include "sdlwrap.h"
#include "perfviz.h"
#include "textrenderer.h"
#include "graphics/GLDebug.h"

#ifdef DEBUG_JOYPAD
#include <list>
#endif

namespace tg
{
	int
	playerIDFromJoypadID(int j_id)
	{
		// TODO(theGiallo, 2015/08/17): this is temporary
		return j_id;
	}
	void
	pauseGameSwitch()
	{
		tg::pauseSwitch();
	}
}

const int major_GL = 3,
          minor_GL = 3;

#ifdef __cplusplus
extern "C"
#endif
int main(int argc, char* argv[])
{
	#if DEBUG
		log_info( "debug version" );
	#else
		log_info( "release version" );
	#endif

	// SDL version
	{
		SDL_version SDL_compiled;
		SDL_version SDL_linked;
		SDL_VERSION(&SDL_compiled);
		SDL_GetVersion(&SDL_linked);
		log_info("Compiled against SDL version %d.%d.%d",
		         SDL_compiled.major, SDL_compiled.minor, SDL_compiled.patch);
		log_info("Linking against SDL version %d.%d.%d",
		         SDL_linked.major, SDL_linked.minor, SDL_linked.patch);
	}

	// SDL_ttf version
	{
		SDL_version SDL_ttf_compiled;
		const SDL_version *SDL_ttf_linked=TTF_Linked_Version();
		SDL_TTF_VERSION(&SDL_ttf_compiled);
		log_info("Compiled against SDL_ttf version: %d.%d.%d", 
		         SDL_ttf_compiled.major, SDL_ttf_compiled.minor, SDL_ttf_compiled.patch);
		log_info("Linking against SDL_ttf version: %d.%d.%d", 
		         SDL_ttf_linked->major, SDL_ttf_linked->minor, SDL_ttf_linked->patch);
	}

	// SDL_mixer version
	{
		SDL_version compile_version;
		const SDL_version *link_version=Mix_Linked_Version();
		SDL_MIXER_VERSION(&compile_version);
		log_info("compiled with SDL_mixer version: %d.%d.%d", 
		         compile_version.major,
		         compile_version.minor,
		         compile_version.patch);
		log_info("running with SDL_mixer version: %d.%d.%d", 
		         link_version->major,
		         link_version->minor,
		         link_version->patch);
	}


	// Initialize
	//---

	// Initialize SDL_ttf
	//---
	if(TTF_Init()==-1)
	{
		log_err("TTF_Init: %s",TTF_GetError());
		return 1;
	} else
	{
		log_info( "SDL_ttf initialized." );
	}

	int ret; // NOTE: used to get the result of functions

	// Initialize SDL's subsystems
	//---
	{
		log_dbg( "Initializing SDL..." );
		ret = SDL_Init( SDL_INIT_VIDEO |
		                SDL_INIT_AUDIO |
		                SDL_INIT_EVENTS |
		                SDL_INIT_JOYSTICK |
		                SDL_INIT_GAMECONTROLLER );
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				log_err( "Could not SDL_Init. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__ );
				// SDL_ClearError();
				return 1;
			}
			log_err( "Could not SDL_Init. No SDL error though. At line #%d of file %s",
			         __LINE__, __FILE__ );
		} else
		{
			log_info("SDL initialized.");
		}
	}

	// Create an application window
	//---
	{
		Environment::window = SDL_CreateWindow(
							APP_NAME,                   // const char* title
							SDL_WINDOWPOS_UNDEFINED,    // int x: initial x position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
							SDL_WINDOWPOS_UNDEFINED,    // int y: initial y position, SDL_WINDOWPOS_CENTERED, or SDL_WINDOWPOS_UNDEFINED
							1920,                       // int w: width, in pixels
							1080,                       // int h: height, in pixels
							  SDL_WINDOW_OPENGL
							| SDL_WINDOW_SHOWN
							| SDL_WINDOW_FULLSCREEN     // Uint32 flags: window options, see docs
		);

		// Check that the window was successfully created
		if(Environment::window==NULL)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				// In the event that the window could not be made...
				log_err( "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
				SDL_Quit();
				return 1;
			}
		} else
		{
			log_info("Window created.");
		}
	}

	// Create OGL context
	//---
	{
		// NOTE: OpenGL core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, major_GL)!=0)
		{
			log_err("Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, %d): %s",
			        major_GL, SDL_GetError());
		}
		if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, minor_GL)!=0)
		{
			log_err("Error on SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, %d): %s",
			        minor_GL, SDL_GetError());
		}
		Environment::glcontext = SDL_GL_CreateContext(Environment::window);
		// Check that the glcontext was successfully created
		if (Environment::glcontext==NULL)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				// In the event that the window could not be made...
				log_err( "Could not create window. SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);
				SDL_Quit();
				return 1;
			}
		} else
		{
			log_info("OpenGL context created.");
		}
	}


	// Some Graphics Settings
	//---
	{
		#if 1
		ret = SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		DCHECKSDLERROR(ret);
		ret = SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		DCHECKSDLERROR(ret);
		ret = SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		DCHECKSDLERROR(ret);
		ret = SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		DCHECKSDLERROR(ret);
		#endif

		#if 1
		ret = SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		DCHECKSDLERROR(ret);

		ret = SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		DCHECKSDLERROR(ret);
		ret = SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
		DCHECKSDLERROR(ret);
		ret = SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		DCHECKSDLERROR(ret);
		#endif


		if(SDL_GL_SetSwapInterval(tg::VSYNC)==-1)
		{
			log_err("Swap interval not supported");
		} else
		{
			if (tg::VSYNC)
				log_info("V-Sync enabled");
			else
				log_info("V-Sync disabled");
		}

		#if 0
		// Settings
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 32);

		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1); 

		// glEnable(GL_MULTISAMPLE);
		ret = SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
		DCHECKSDLERROR(ret);
		#endif

		int Buffers, Samples;
		SDL_GL_GetAttribute( SDL_GL_MULTISAMPLEBUFFERS, &Buffers );
		SDL_GL_GetAttribute( SDL_GL_MULTISAMPLESAMPLES, &Samples );
		std::cout << "buf = " << Buffers << ", samples = " << Samples << "."<<std::endl;
	}

	// Init GLEW
	//---
	{
		/**
		 * NOTE:
		 * From https://www.opengl.org/wiki/OpenGL_Loading_Library
		 *
		 * GLEW has a problem with core contexts. It calls
		 * glGetString(GL_EXTENSIONS)​, which causes GL_INVALID_ENUM​ on GL 3.2+ core
		 * context as soon as glewInit()​ is called. It also doesn't fetch the
		 * function pointers. The solution is for GLEW to use glGetStringi​ instead.
		 * The current version of GLEW is 1.10.0 but they still haven't corrected it.
		 * The only fix is to use glewExperimental​ for now:
		 *
		 *	  glewExperimental=TRUE;
		 *	  GLenum err=glewInit();
		 *	  if(err!=GLEW_OK)
		 *	  {
		 *		//Problem: glewInit failed, something is seriously wrong.
		 *		cout<<"glewInit failed, aborting."<<endl;
		 *	  }
		 *
		 *	glewExperimental​ is a variable that is already defined by GLEW. You must
		 * set it to GL_TRUE before calling glewInit()​.
		 *	You might still get GL_INVALID_ENUM (depending on the version of GLEW you
		 * use), but at least GLEW ignores glGetString(GL_EXTENSIONS) and gets all
		 * function pointers. If you are creating a GL context the old way or if you
		 * are creating a backward compatible context for GL 3.2+, then you don't
		 * need glewExperimental.
		 *	As with most other loaders, you should not include gl.h​, glext.h​, or any
		 * other gl related header file before glew.h​, otherwise you'll get an error
		 * message that you have included gl.h​ before glew.h​. You shouldn't be
		 * including gl.h​ at all; glew.h​ replaces it.
		 *	GLEW also provides wglew.h​ which provides Windows specific GL functions
		 * (wgl functions). If you include wglext.h​ before wglew.h​, GLEW will
		 * complain. GLEW also provides glxew.h​ for X windows systems. If you include
		 * glxext.h​ before glxew.h​, GLEW will complain.
		 **/
		glewExperimental = GL_TRUE;
		DDEBUG_GLERRORCHECK();
		GLenum err = glewInit(); // NOTE: read the huge comment above!
		if (DDEBUG_GLERRORCHECK()!=0)
		{
			log_info("The above error is ok. Do not worry.");
		}
		if (GLEW_OK!=err)
		{
			log_err("Error initializing GLEW: %s", glewGetErrorString(err));
		}
		if (GLEW_VERSION_1_3)
		{
			log_info("GLEW 1.3");
		}

		log_info("GLEW version: %d.%d.%d",
		         GLEW_VERSION_MAJOR,GLEW_VERSION_MINOR,GLEW_VERSION_MICRO);
	}



	// Initialize SDL_mixer
	//---
	{
		// load support for the MP3 and MOD sample/music formats
		int flags=MIX_INIT_OGG;
		int initted=Mix_Init( flags );
		if ((initted & flags) != flags)
		{
			log_info( "Mix_Init: Failed to init required ogg support!\n" );
			log_info( "Mix_Init: %s\n", Mix_GetError() );
			// TODO: handle error
		} else
		{
			log_info( "SDL_mixer initialized." );
		}
	}

	// Configure SDL_mixer
	//---
	{
		// int Mix_OpenAudio(int frequency, Uint16 format, int channels, int chunksize)
		if (Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 1024 )==-1)
		{
			log_err( "Mix_OpenAudio: %s\n", Mix_GetError() );
			if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024)==-1)
			{
				log_err( "Mix_OpenAudio: %s\n", Mix_GetError() );
			} else
			{
				log_info( "SDL Mixer audio opened." );
			}
		} else
		{
			log_info("SDL Mixer audio opened.");
		}
		// get and print the audio format in use
		int numtimesopened, frequency, channels;
		Uint16 format;
		numtimesopened=Mix_QuerySpec(&frequency, &format, &channels);
		if (!numtimesopened)
		{
			log_err("Mix_QuerySpec: %s",Mix_GetError());
		} else
		{
			const char *format_str="Unknown";
			switch (format)
			{
				case AUDIO_U8:     format_str="U8";     break;
				case AUDIO_S8:     format_str="S8";     break;
				case AUDIO_U16LSB: format_str="U16LSB"; break;
				case AUDIO_S16LSB: format_str="S16LSB"; break;
				case AUDIO_U16MSB: format_str="U16MSB"; break;
				case AUDIO_S16MSB: format_str="S16MSB"; break;
			}
			log_info("opened=%d times  frequency=%dHz  format=%s  channels=%d",
			         numtimesopened, frequency, format_str, channels);
		}
		// print music decoders available
		int i,max=Mix_GetNumMusicDecoders();
		for (i=0; i<max; ++i)
		{
			log_info( "Music decoder %d is for %s\n", i, Mix_GetMusicDecoder( i ) );
		}
	}

	// Load the music
	//---
	Mix_Music * music;
	{
		music = Mix_LoadMUS( "audio/music/main_music.ogg" );
		if(!music)
		{
			log_err( "Mix_LoadMUS(\"audio/music/main_music.ogg\"): %s\n", Mix_GetError());
			// this might be a critical error...
		} else
		{	// play music forever
			if(Mix_PlayMusic(music, -1)==-1)
			{
				log_err( "Mix_PlayMusic: %s\n", Mix_GetError());
				// well, there's no music, but most games don't break without music...
			}
		}
	}

	// Allocate sound effects channels
	//---
	{
		const int numchans = 1000;
		int actual_numchans = Mix_AllocateChannels(numchans);
		if (numchans != actual_numchans)
		{
			log_err( "Requested %d audio channels, but %d were allocated\n", numchans, actual_numchans);
		} else
		{
			log_info("SDL Mixer channels allocated.");
		}
	}

	// Load sound effects
	//---
	// NOTE: was done by Sounds::init(), TODO(theGiallo, 2015/08/17): need to re-add it
	//------ end mixer --------


	// Program life cycle
	//---

	SDL_Event event;
	tg::initEngine();
	Time old_time = tg::getTimeInSec();
	Time game_time = 0.0,
	     delta_time = 0.0;
	double alpha;
	int n_steps=0;
	tgmath::i::Vec2 mouse_pos;

	Environment::init();

	tg::PerfViz perf_viz_frame_time;
	perf_viz_frame_time._buffer_size = tg::MAX_FPS * 10;
	perf_viz_frame_time.init();
	perf_viz_frame_time.width = 1200;
	perf_viz_frame_time.height = 120;
	perf_viz_frame_time.centerHorizontally();
	perf_viz_frame_time.distFromBottom(24);
	perf_viz_frame_time.samples_visualized = perf_viz_frame_time.width / 4;
	perf_viz_frame_time.max_value = 1.0 / 100.0;
	perf_viz_frame_time.rendered = false;

	// NOTE: test animations
	#if TEST_ANIMATIONS
		int ww, wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		SDLWrap_Texture atlas;
		std::string atlas_path = "imgs/atlas.bmp";
		loadTexture(atlas_path, atlas);
		tg::AnimatedSprite animated_sprite;
		animated_sprite.init();
		animated_sprite.texture_atlas = atlas;
		animated_sprite.frame_size = {48,48};
		animated_sprite.atlas_size_in_frames = {10,7};
		animated_sprite.start_frame_ID = 33;
		animated_sprite.end_frame_ID = 39;
		animated_sprite.pos = {ww/2, wh/2};
		animated_sprite.duration = 2.0;
		animated_sprite.total_loops_to_play = 2;
		animated_sprite.die_at_end = true;
		tg::paused = false;
	#endif

#ifdef DEBUG_JOYPAD
	int debug_button_pressed = 0;
	std::list<std::pair<int,int>> debug_axis_moved;
#endif

	// Game controller handlers
	//---
	log_info( "Starting controllers configuration");
	// NOTE: moved in Environment
	/*
	std::vector<SDL_GameController *> game_controllers;
	std::map<int,int> gc_which; // NOTE: gc_which[joystick_instanceID] = joystick_number;
	*/
	int valid_controllers = 0;
	// Check for joysticks
	//---
	
	#ifdef ANDROID
	// Add mapping for Xbox 360 wireless pads on Android Lollipop
	ret = SDL_GameControllerAddMapping("58626f782033363020576972656c6573,Xbox 360 Wireless Controller,a:b0,b:b1,x:b2,y:b3,start:b6,leftstick:b7,rightstick:b8,leftshoulder:b9,rightshoulder:b10,dpup:b11,dpdown:b12,dpleft:b13,dpright:b14,leftx:a0,lefty:a1,rightx:a2,righty:a3,lefttrigger:a4,righttrigger:a5"); // MISSING back; does he wants triggers span?
					log_info( "SDL_GameControllerAddMapping ret = %d", ret);
	#endif

	if (SDL_NumJoysticks() < 1)
	{
		std::cerr<<"Warning: No joysticks connected!"<<std::endl;
	} else
	{
		Environment::game_controllers.resize(SDL_NumJoysticks());
		log_info( "%d joysticks connected",SDL_NumJoysticks());
		for (int i=0; i<SDL_NumJoysticks(); i++)
		{
			//Load joystick
			if (SDL_IsGameController(i))
			{
				Environment::game_controllers[i] = SDL_GameControllerOpen(i);
				if(Environment::game_controllers[i] == nullptr)
				{
					std::cout<<"Warning: Unable to open game controller! SDL Error: "<<SDL_GetError()<<std::endl;
					continue;
				}

				log_info( "Found a valid controller, id=%d named: %s", i, SDL_GameControllerName(Environment::game_controllers[i]));

				SDL_Joystick *joy = SDL_GameControllerGetJoystick( Environment::game_controllers[i] );
				int instanceID = SDL_JoystickInstanceID( joy );
				Environment::gc_which[instanceID] = i;
				log_info( "                  instanceID=%d", instanceID);
				valid_controllers++;
			} else
			{
				log_info( "joystick %d is not a game controller [%s]", i, SDL_GameControllerNameForIndex(i));
				log_info( "joystick %d name: %s", i, SDL_JoystickNameForIndex(i));
				Environment::game_controllers[i] = nullptr;
			}
			int cbGUID = 1000;
			char pszGUID[cbGUID]; pszGUID[0] = 0;
			SDL_JoystickGetGUIDString(SDL_JoystickGetDeviceGUID(i), pszGUID, cbGUID);
			log_info( "pszGUID: %s",pszGUID);
		}
	}
	log_info( "Controllers configuration completed");

	// Fonts
	//---
	Font default_font = Font::getFont(Font("fonts/Inconsolata/Inconsolata-Regular.ttf", 16 * Environment::zoom));
	Font::debug_info_font = default_font;
	// load font .ttf at size 16 into font
	TTF_Font *font = default_font.getFont();
	if(!font)
	{
		log_err( "TTF_OpenFont: %s\n", TTF_GetError());
		return 2;
	}

	{
	// get the loaded font's hinting setting
	int hinting=TTF_GetFontHinting(font);
	log_info("The font hinting is currently set to %s\n",
			hinting==TTF_HINTING_NORMAL?"Normal":
			hinting==TTF_HINTING_LIGHT?"Light":
			hinting==TTF_HINTING_MONO?"Mono":
			hinting==TTF_HINTING_NONE?"None":
			"Unknonwn");
	}

	// set the loaded font's hinting to optimized for monochrome rendering
	TTF_SetFontHinting(font, TTF_HINTING_MONO);
	// set the loaded font's hinting to optimized for monochrome rendering
	TTF_SetFontHinting(font, TTF_HINTING_NORMAL);

	{
	// get the loaded font's kerning setting
	int kerning = TTF_GetFontKerning(font);
	log_info("The font kerning is currently set to %s\n",
			kerning==0?"Off":"On");
	}
	
	// turn off kerning on the loaded font
	TTF_SetFontKerning(font, 0);

	// turn on kerning on the loaded font
	TTF_SetFontKerning(font, 1);


	while (tg::running)
	{
		if (tg::has_to_start)
		{
			tg::has_to_start = false;

			// NOTE: restart
		}

		#if PRINTFPS
			log_info("---------");
		#endif

		// Calc past spent time
		//---
		
		Time current_time = tg::getTimeInSec();
		Time frame_duration = current_time - old_time;
		Time clamped_frame_duration = frame_duration;
		Time real_fd = frame_duration;
		tg::ms_per_frame = frame_duration*1000.0f;
		perf_viz_frame_time.addSample(real_fd);
		#if PRINTFPS
			log_info("frame_duration %fms", frame_duration*1000);
			log_info("instant freq %fHz", 1.0/frame_duration);
		#endif
		if (frame_duration > tg::MAX_TIME)
		{
			clamped_frame_duration = tg::MAX_TIME;
		} else
		if (frame_duration < tg::MIN_TIME)
		{
			double ms_to_wait = (tg::MIN_TIME - frame_duration) * 1000.0;
			#if PRINTFPS
				log_info("sleep for %fms",ms_to_wait);
			#endif

			// minimum guaranteed sleep time is 9/10ms, SDL knows this but with it we sleep ~0.1ms more (on the machine of the author)
			if (ms_to_wait>=10)
			{
				// sleep for >=10ms
				SDL_Delay(static_cast<int>(ms_to_wait));

				// busy waiting for <1ms
				double start=tg::getTimeInSec(),i;
				// int it=0;
				ms_to_wait = MODF(ms_to_wait,&i);
				// log_info("b.w. %f",ms_to_wait);
				while ((tg::getTimeInSec()-start)*1000.0 < ms_to_wait)
				{
					// it++;
				};
				// log_info("b.w. for %d iterations",it);
			} else
			{
				//SDL_Delay(ms_to_wait); // try yourself uncommenting whis line and commenting the b.w.
				// busy waiting for max 10ms
				while ((tg::getTimeInSec()-current_time)*1000.0 < ms_to_wait){};
			}
			current_time = tg::getTimeInSec();
			real_fd = current_time - old_time;
			#if PRINTFPS
				log_info("real_fd %fms", real_fd*1000);
				log_info("real instant freq %fHz", 1.0/real_fd);
			#endif
			clamped_frame_duration = tg::MIN_TIME;
		}

		tg::fps = 1.0/real_fd;
		old_time = current_time;
		tg::time_accumulator += clamped_frame_duration;
		#if PRINTFPS
			log_info("clamped %fms", clamped_frame_duration*1000);
			log_info("time_accumulator %fms", tg::time_accumulator*1000);
		#endif


		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);


		// Get Keymap state
		//---
		int nk;
		const Uint8* keys = SDL_GetKeyboardState(&nk);

		// Event management
		//---
		while (SDL_PollEvent(&event))
		{
			switch ( event.type )
			{
			case SDL_WINDOWEVENT:
				switch (event.window.event)
				{
				case SDL_WINDOWEVENT_CLOSE:
					log_info("Window %d closed", event.window.windowID);
					tg::running=false;
					break;
				default:
					break;
				}
				break;
			case SDL_MOUSEBUTTONUP:
			{
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					break;
				case SDL_BUTTON_RIGHT:
					break;
				case SDL_BUTTON_MIDDLE:
					break;
				case SDL_BUTTON_X1:
					break;
				case SDL_BUTTON_X2:
					break;
				default:
					assert(false);
					break;
				}
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			{
				switch (event.button.button)
				{
				case SDL_BUTTON_LEFT:
					break;
				case SDL_BUTTON_RIGHT:
					break;
				case SDL_BUTTON_MIDDLE:
					break;
				case SDL_BUTTON_X1:
					break;
				case SDL_BUTTON_X2:
					break;
				default:
					assert(false);
					break;
				}
				break;
			}
			case SDL_MOUSEMOTION:
			{
				mouse_pos.setXY(event.motion.x, wh - event.motion.y);
				break;
			}
			case SDL_KEYUP:
			{
				switch (event.key.keysym.sym)
				{
					case SDLK_r:
					break;
					case SDLK_s:
					break;
					case SDLK_t:
					break;
					default:
					break;
				}
				break;
			}
			case SDL_KEYDOWN:
			{
				switch (event.key.keysym.sym)
				{
					case SDLK_r:
						if (tg::paused)
						{
							tg::has_to_start = true;
						}
						break;
					case SDLK_UP:
						break;
					case SDLK_DOWN:
						break;
					case SDLK_LEFT:
						break;
					case SDLK_RIGHT:
						break;
					case SDLK_SPACE:
					case SDLK_RETURN:
						break;
					case SDLK_BACKSPACE:
						break;
					case SDLK_ESCAPE:
						tg::running=false;
						break;
						if (0 == event.key.repeat%2)
						{
							tg::pauseGameSwitch();
						}
						break;
					case SDLK_PAGEDOWN:
						break;
					case SDLK_PAGEUP:
						break;
					case SDLK_F1:
						perf_viz_frame_time.rendered = !perf_viz_frame_time.rendered;
						break;
					case SDLK_0:
						break;
					case SDLK_1:
						break;
					default:
						break;
				}
				break;
			}

			//---------- start controller events ----------------------------------
			case SDL_EventType::SDL_CONTROLLERAXISMOTION:
			{
				int pid = tg::playerIDFromJoypadID(event.caxis.which);
				if (pid==-1) // joystick is not a valid game controller and is not associated to any plane
					break;
				switch (event.caxis.axis)
				{
					case SDL_CONTROLLER_AXIS_INVALID:
						break;
					case SDL_CONTROLLER_AXIS_LEFTX:
					{
						Environment::left_stick[pid].x = event.jaxis.value;
						if (Environment::left_stick[pid].getModule() > JOYAXIS_LOWLIMIT)
						{
							float a = tgmath::f::RAD2DEG(atan2(Environment::left_stick[pid].y,Environment::left_stick[pid].x));
						}
					}
					break;
					case SDL_CONTROLLER_AXIS_LEFTY:
					{
						Environment::left_stick[pid].y = -event.jaxis.value;
						if (Environment::left_stick[pid].getModule() > JOYAXIS_LOWLIMIT)
						{
							float a = tgmath::f::RAD2DEG(atan2(Environment::left_stick[pid].y,Environment::left_stick[pid].x));
						}
					}
					break;
					case SDL_CONTROLLER_AXIS_RIGHTX:
					{
						Environment::right_stick[pid].x = event.jaxis.value;
						float stick_module = Environment::right_stick[pid].getModule();
						if (stick_module >= 32000.0f)
						{
							// NOTE: button like behavior
						}
						if (stick_module > JOYAXIS_LOWLIMIT)
						{
							float a = tgmath::f::RAD2DEG(atan2(Environment::right_stick[pid].y,Environment::right_stick[pid].x));
						}
					}
					break;
					case SDL_CONTROLLER_AXIS_RIGHTY:
					{
						Environment::right_stick[pid].y = -event.jaxis.value;
						float stick_module = Environment::right_stick[pid].getModule();
						if (stick_module >= 32000.0f)
						{
							// NOTE: button like behavior
						}
						if (stick_module > JOYAXIS_LOWLIMIT)
						{
							float a = tgmath::f::RAD2DEG(atan2(Environment::right_stick[pid].y,Environment::right_stick[pid].x));
						}
					}
					break;
					case SDL_CONTROLLER_AXIS_TRIGGERLEFT:
					{
						Environment::left_trigger[pid] = event.jaxis.value;
						if (Environment::left_trigger[pid]==1)
							Environment::left_trigger[pid] = 0;
					}
					break;
					case SDL_CONTROLLER_AXIS_TRIGGERRIGHT:
						break;
					case SDL_CONTROLLER_AXIS_MAX:
						break;
				}

				break;
			}
			case SDL_EventType::SDL_CONTROLLERBUTTONDOWN:
			{
				int pid = tg::playerIDFromJoypadID(event.cbutton.which);
				if (pid==-1)
				// joystick is not associated to any plane and we are not choosing players 
				{
					break;
				}

				// log_info( "controller button of controller #%d",gc_which[event.caxis.which]);
				switch (event.cbutton.button)
				{
					case SDL_CONTROLLER_BUTTON_INVALID:
						log_err("Invalid controller button pressed!");
						break;
					case SDL_CONTROLLER_BUTTON_A:
						break;
					case SDL_CONTROLLER_BUTTON_B:
						break;
					case SDL_CONTROLLER_BUTTON_X:
						break;
					case SDL_CONTROLLER_BUTTON_Y:
						break;
					case SDL_CONTROLLER_BUTTON_BACK:
						break;
					case SDL_CONTROLLER_BUTTON_GUIDE:
						break;
					case SDL_CONTROLLER_BUTTON_START:
						break;
					case SDL_CONTROLLER_BUTTON_LEFTSTICK:
						break;
					case SDL_CONTROLLER_BUTTON_RIGHTSTICK:
						break;
					case SDL_CONTROLLER_BUTTON_LEFTSHOULDER:
						break;
					case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_UP:
						#ifdef DEBUG
							perf_viz_frame_time.rendered = true;
						#endif
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
						#ifdef DEBUG
							perf_viz_frame_time.rendered = false;
						#endif
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
						break;
					case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
						break;
					case SDL_CONTROLLER_BUTTON_MAX:
						break;
					default:
						break;
				}
				break;
			}
			//---------- end controller events ------------------------------------

			} // switch eventType
		} // pollEvent

		// end input
		//------------------------------------------------------------------------


		// NOTE: time_accumulator could have been modified during input processing
		// in case of pause switching
		n_steps = static_cast<int>(tg::time_accumulator/tg::FIXED_TIMESTEP);
#if PRINTFPS
		log_info("n_steps %d", n_steps);
#endif

		// Fixed time-step update
		//---
		if (tg::paused)
		{

		} else
		{
			if (n_steps > 0)
			{
				tg::time_accumulator -= n_steps*tg::FIXED_TIMESTEP;
				#if PRINTFPS
				log_info("over %fms", tg::time_accumulator*1000);
				#endif

				for (int s=0; s!=n_steps; s++)
				{
					/**
					 * for each element
					 * previous_state = current_state
					 * fixedTSUpdate(game_time, tg::FIXED_TIMESTEP)
					 **/

					game_time += tg::FIXED_TIMESTEP;
				}
			}
			#if PRINTFPS
				log_info("time_accumulator %fms", tg::time_accumulator*1000);
			#endif

			alpha = tg::time_accumulator / tg::FIXED_TIMESTEP;
			delta_time = n_steps*tg::FIXED_TIMESTEP;


			// Time dependant update
			//---

			/**
			 * for each element
			 * update(game_time, n_steps*tg::FIXED_TIMESTEP)
			 **/

			// Smooth
			//---

			/**
			 * for each element
			 * current_state = current_state * alpha + previous_state * (1.0 - alpha)
			 */
			// smoothToRenderState(alpha);
		}


		// Render
		//---

		// tg::GUI::render();
		perf_viz_frame_time.render();

		#ifdef DEBUG_JOYPAD
			Font::renderText(tg::ms<<
					"YOU PRESSED  - THE -  BUTTON  "<<debug_button_pressed,
					default_font, ww/3, wh/2, {255,0,0}, 255);
			int tmp = 20;
			for (std::pair<int,int> id : debug_axis_moved)
			{
				Font::renderText(tg::ms<<
				    "YOU MOVED  - THE -  AXIS  "<<id.first<<" = "<<id.second,
				    default_font, ww/3, wh/2-tmp, {255,0,0}, 255);
				tmp += 20;
			}
			while (debug_axis_moved.size()>10)
			{
				debug_axis_moved.pop_back();
			}
		#endif
		
		// update the screen with the performed rendering
		SDL_GL_SwapWindow(Environment::window);
	}


	// Exit
	//---
	log_info( "Exiting...");

	// TODO(theGiallo, 2015/08/17): move into Environment
	if (Environment::window)
	{
		SDL_DestroyWindow(Environment::window);
	}
	if (Environment::glcontext)
	{
		SDL_GL_DeleteContext(Environment::glcontext);
	}


	// Close and destroy the window
	// SDL_DestroyWindow(Environment::window); // commented because it make the resolution not to restore from fullscreen

	// Quit SDL
	SDL_Quit();

	// Close the font
	TTF_CloseFont(font);

	// Quit SDL_ttf
	if (TTF_WasInit())
	{
		TTF_Quit();
	}
	return 0;
}