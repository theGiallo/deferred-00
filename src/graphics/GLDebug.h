/* 
 * File:   GLDebug.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 16 febbraio 2011, 1.48
 */

#ifndef GLDEBUG_H
#define	GLDEBUG_H
#ifdef WIN32
	#include <windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
int CheckGLError(const char *file, int line);

#ifdef DEBUG_GL
	#define DDEBUG_GLERRORCHECK() CheckGLError(__FILE__, __LINE__)
#else
	#define DDEBUG_GLERRORCHECK() 0
#endif


#ifdef DEBUG
#define DCHECKSDLERROR(ret) if (ret != 0)\
	{\
		const char *error = SDL_GetError();\
		if (*error != '\0')\
		{\
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);\
			SDL_ClearError();\
		}\
	}
#else
#define DCHECKSDLERROR(ret)
#endif

#endif	/* GLDEBUG_H */

