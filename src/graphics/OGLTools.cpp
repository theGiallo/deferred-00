#include "OGLTools.h"
#include "GLDebug.h"

#include <iostream>
#include <cassert>

OGLTools::OGLTools()
{
}

OGLTools::Program * OGLTools::current_program = nullptr;

// TODO: according to Chandler Carruth this 3 func. could be inlined if they
// returned by value and not by an out param (and get to better performance).
void OGLTools::translateVArray(GLfloat* vArray, unsigned int count, const Vec2& t)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        vArray[i] += t.x;
        vArray[i+1] += t.y;
    }
}
void OGLTools::rotateVArrayDeg(GLfloat* vArray, unsigned int count, float rot)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        Vec2 res = Vec2(vArray[i],vArray[i+1]).getRotatedDeg(rot);
        vArray[i] = res.x;
        vArray[i+1] = res.y;
    }
}
void OGLTools::scaleVArray(GLfloat* vArray, unsigned int count, const Vec2& s)
{
    for (unsigned int i=0; i<2*count ; i+=2)
    {
        vArray[i] *= s.x;
        vArray[i+1] *= s.y;
    }
}


void OGLTools::createVBO_siv(VBO_iv & vbo)
{
	vbo.i_type = GL_UNSIGNED_SHORT;
	vbo.i_size = sizeof(GLushort);
	vbo.vertices = new std::vector<tgmath::f::Vec3>();
	vbo.s.indices = new std::vector<GLushort>();
}
void OGLTools::createVBO_liv(VBO_iv & vbo)
{
	vbo.i_type = GL_UNSIGNED_INT;
	vbo.i_size = sizeof(GLuint);
	vbo.vertices = new std::vector<tgmath::f::Vec3>();
	vbo.l.indices = new std::vector<GLuint>();
}
void OGLTools::deleteVBO_iv(VBO_iv & vbo)
{
	switch (vbo.i_type)
	{
		case GL_UNSIGNED_SHORT:
			deleteVBO_siv(vbo);
			break;
		case GL_UNSIGNED_INT:
			deleteVBO_liv(vbo);
			break;
	}
}
void OGLTools::deleteVBO_siv(VBO_iv & vbo)
{
	if (vbo.vertices)
		delete vbo.s.vertices;
	if (vbo.indices)
		delete vbo.s.indices;
}
void OGLTools::deleteVBO_liv(VBO_iv & vbo)
{
	if (vbo.vertices)
		delete vbo.l.vertices;
	if (vbo.indices)
		delete vbo.l.indices;
}

void OGLTools::destroyVBO(VBO_iv & vbo)
{
	glDeleteBuffers(1,&vbo.vbuf);
	glDeleteBuffers(1,&vbo.ibuf);
}
void OGLTools::destroyProgramAndShaders(Program & program)
{
	glDeleteProgram(program.program);
	glDeleteShader(program.vs);
	glDeleteShader(program.fs);
}

bool OGLTools::createProgram(Program & program,
	                          GLuint vs_count,
	                          const GLchar ** vs_strings,
	                          const GLint * vs_lengths,
	                          GLuint fs_count,
	                          const GLchar ** fs_strings,
	                          const GLint * fs_lengths,
	                          GLuint gs_count,
	                          const GLchar ** gs_strings,
	                          const GLint * gs_lengths)
	{
		program.program = glCreateProgram();
		if (program.program == 0)
		{
			// TODO: output error
			return false;
		}

		if (vs_count)
		{
			GLuint vs = glCreateShader(GL_VERTEX_SHADER);
			if (vs == 0)
			{
				// TODO: output error
				return false;
			}
			glShaderSource(vs,          // shader
			               vs_count,    // count
			               vs_strings,  // string
			               vs_lengths); // length
			glCompileShader(vs);
			glAttachShader(program.program, vs);
			program.vs = vs;

			// Check Vertex Shader
			GLint Result = GL_FALSE;
			int InfoLogLength;
			glGetShaderiv(program.vs, GL_COMPILE_STATUS, &Result);
			if (Result == GL_FALSE)
			{
				std::cerr<<"Error compiling vertex shader at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
			}
			glGetShaderiv(program.vs, GL_INFO_LOG_LENGTH, &InfoLogLength);
			if ( InfoLogLength > 0 ){
				std::vector<char> ShaderErrorMessage(InfoLogLength+1);
				glGetShaderInfoLog(program.vs, InfoLogLength, NULL, &ShaderErrorMessage[0]);
				std::cout<<ShaderErrorMessage.data()<<std::endl;
			}
		}


		if (gs_count)
		{
			GLuint gs = glCreateShader(GL_GEOMETRY_SHADER);
			if (gs == 0)
			{
				// TODO: output error
				return false;
			}
			glShaderSource(gs,          // shader
			               gs_count,    // count
			               gs_strings,  // string
			               gs_lengths); // length
			DDEBUG_GLERRORCHECK();
			glCompileShader(gs);
			DDEBUG_GLERRORCHECK();
			glAttachShader(program.program, gs);
			DDEBUG_GLERRORCHECK();
			program.gs = gs;

			// Check Geometry Shader
			GLint Result = GL_FALSE;
			GLint InfoLogLength;
			glGetShaderiv(program.gs, GL_COMPILE_STATUS, &Result);
			if (Result == GL_FALSE)
			{
				std::cerr<<"Error compiling geometry shader at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
				std::vector<char> ShaderErrorMessage(1000);
				glGetShaderInfoLog(program.vs, 1000, NULL, &ShaderErrorMessage[0]);
				std::cout<<ShaderErrorMessage.data()<<std::endl;
			}
			glGetShaderiv(program.gs, GL_INFO_LOG_LENGTH, &InfoLogLength);
			if ( InfoLogLength > 0 ){
				std::vector<char> ShaderErrorMessage(InfoLogLength+1);
				ShaderErrorMessage[InfoLogLength] = 0;
				glGetShaderInfoLog(program.gs, InfoLogLength, NULL, &ShaderErrorMessage[0]);
				std::cout<<ShaderErrorMessage.data()<<std::endl;
			}
		}

		if (fs_count)
		{
			GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
			if (fs == 0)
			{
				// TODO: output error
				return false;
			}
			glShaderSource(fs,          // shader
			               fs_count,    // count
			               fs_strings,  // string
			               fs_lengths); // length
			glCompileShader(fs);
			glAttachShader(program.program, fs);
			program.fs = fs;

			// Check Fragment Shader
			GLint Result = GL_FALSE;
			int InfoLogLength;
			glGetShaderiv(program.fs, GL_COMPILE_STATUS, &Result);
			if (Result == GL_FALSE)
			{
				std::cerr<<"Error compiling fragment shader at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
			}
			glGetShaderiv(program.fs, GL_INFO_LOG_LENGTH, &InfoLogLength);
			if ( InfoLogLength > 0 ){
				std::vector<char> ShaderErrorMessage(InfoLogLength+1);
				glGetShaderInfoLog(program.fs, InfoLogLength, NULL, &ShaderErrorMessage[0]);
				std::cout<<ShaderErrorMessage.data()<<std::endl;
			}
		}

		glLinkProgram(program.program);
		DDEBUG_GLERRORCHECK();

		glValidateProgram(program.program);
		DDEBUG_GLERRORCHECK();

		// Check the program
		{
		GLint Result = GL_FALSE;
		int InfoLogLength;
		glGetProgramiv(program.program, GL_LINK_STATUS, &Result);
		glGetProgramiv(program.program, GL_INFO_LOG_LENGTH, &InfoLogLength);
		if ( InfoLogLength > 0 )
		{
			std::vector<char> ProgramErrorMessage(InfoLogLength+1);
			glGetProgramInfoLog(program.program, InfoLogLength, NULL, &ProgramErrorMessage[0]);
			std::cout<<ProgramErrorMessage.data()<<std::endl;
		}
		}

		// detach shaders
		if (program.vs) glDetachShader(program.program, program.vs);
		if (program.gs) glDetachShader(program.program, program.gs);
		if (program.fs) glDetachShader(program.program, program.fs);

		// delete shaders
		if (program.vs) glDeleteShader(program.vs);
		if (program.gs) glDeleteShader(program.gs);
		if (program.fs) glDeleteShader(program.fs);


		GLint a_i;

		a_i = glGetAttribLocation(program.program, "vertex_pos_modelspace");
		DDEBUG_GLERRORCHECK();
		program.va_vertices = 0;
		if (a_i!=-1)
		{
			program.va_vertices = a_i;
		} else
		{
			std::cout<<"no vertex attribute vertex_pos_modelspace"<<std::endl;
		}

		a_i = glGetAttribLocation(program.program, "vertexUV");
		DDEBUG_GLERRORCHECK();
		program.va_uvs = 0;
		if (a_i!=-1)
		{
			program.va_uvs = a_i;
		} else
		{
			std::cout<<"no uniform vertexUV"<<std::endl;
		}

		 a_i = glGetAttribLocation(program.program, "normal_modelspace");
		DDEBUG_GLERRORCHECK();
		program.va_normals = 0;
		if (a_i!=-1)
		{
			program.va_normals = a_i;
		} else
		{
			std::cout<<"no uniform normal_modelspace"<<std::endl;
		}

		GLint u_i;

		u_i = glGetUniformLocation(program.program, "MVP");
		DDEBUG_GLERRORCHECK();
		program.u_MVP = 0;
		if (u_i!=-1)
		{
			program.u_MVP = u_i;
		} else
		{
			std::cout<<"no uniform MVP"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "M");
		DDEBUG_GLERRORCHECK();
		program.u_M = 0;
		if (u_i!=-1)
		{
			program.u_M = u_i;
		} else
		{
			std::cout<<"no uniform M"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "V");
		DDEBUG_GLERRORCHECK();
		program.u_V = 0;
		if (u_i!=-1)
		{
			program.u_V = u_i;
		} else
		{
			std::cout<<"no uniform V"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "P");
		DDEBUG_GLERRORCHECK();
		program.u_P = 0;
		if (u_i!=-1)
		{
			program.u_P = u_i;
		} else
		{
			std::cout<<"no uniform P"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "LightPosition_worldspace");
		DDEBUG_GLERRORCHECK();
		program.u_light_pos = 0;
		if (u_i!=-1)
		{
			program.u_light_pos = u_i;
		} else
		{
			std::cout<<"no uniform LightPosition_worldspace"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "LightPower");
		DDEBUG_GLERRORCHECK();
		program.u_light_power = 0;
		if (u_i!=-1)
		{
			program.u_light_power = u_i;
		} else
		{
			std::cout<<"no uniform LightPower"<<std::endl;
		}

		u_i = glGetUniformLocation(program.program, "myTextureSampler");
		DDEBUG_GLERRORCHECK();
		program.u_texture = 0;
		if (u_i!=-1)
		{
			program.u_texture = u_i;
		} else
		{
			std::cout<<"no uniform myTextureSampler"<<std::endl;
		}

		return (DDEBUG_GLERRORCHECK() == 0);
	}

/**
 * To be called once before drawVBO
 **/
bool OGLTools::initVBO(VBO_iv & vbo)
{
	glGenBuffers(1, &vbo.vbuf);
	DDEBUG_GLERRORCHECK();
	if (vbo.vbuf==0)
	{
		std::cerr<<"vertices buffer not created!"<<std::endl;
		DDEBUG_GLERRORCHECK();
	}
	glBindBuffer(GL_ARRAY_BUFFER, vbo.vbuf);
	DDEBUG_GLERRORCHECK();
	glBufferData(GL_ARRAY_BUFFER,                              // target
	             vbo.vertices->size()*sizeof(tgmath::f::Vec3), // size
	             vbo.vertices->data(),                         // data
	             GL_DYNAMIC_DRAW);                              // usage // NOTE: or GL_STATIC_DRAW
	DDEBUG_GLERRORCHECK();


	glGenBuffers(1, &vbo.ibuf);
	DDEBUG_GLERRORCHECK();
	if (vbo.ibuf==0)
	{
		std::cerr<<"indices buffer not created!"<<std::endl;
		DDEBUG_GLERRORCHECK();
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.ibuf);
	DDEBUG_GLERRORCHECK();
	if (vbo.ibuf==0)
	{
		std::cerr<<"indices buffer not created!"<<std::endl;
		DDEBUG_GLERRORCHECK();
	}
	GLuint size;
	void * data;
	switch (vbo.i_type)
	{
		case GL_UNSIGNED_SHORT:
			size = vbo.s.indices->size();
			data = vbo.s.indices->data();
			break;
		case GL_UNSIGNED_INT:
			size = vbo.l.indices->size();
			data = vbo.l.indices->data();
			break;
	}
	size *= vbo.i_size;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,      // target
	             size,                         // size
	             data,                         // data
	             GL_STATIC_DRAW);              // usage
	DDEBUG_GLERRORCHECK();
	if (vbo.ibuf==0)
	{
		std::cerr<<"indices buffer not created!"<<std::endl;
		DDEBUG_GLERRORCHECK();
	}


	return (DDEBUG_GLERRORCHECK() == 0);
}
/**
 * Call initVBO once before drawVBO!
 * Call glUseProgram(GLuint) at least once before drawVBO!
 * Be sure that current_program is correctly set
 * Be sure that all uniforms are set
 **/
bool OGLTools::drawVBO(const VBO_iv & vbo)
{
	GLuint va_vertices = current_program->va_vertices;
	glEnableVertexAttribArray(va_vertices);
	DDEBUG_GLERRORCHECK();
	glBindBuffer(GL_ARRAY_BUFFER, vbo.vbuf);
	DDEBUG_GLERRORCHECK();
	glVertexAttribPointer(va_vertices, // index
	                      3,           // size
	                      GL_FLOAT,    // type
	                      GL_FALSE,    // normalized
	                      0,           // stride
	                      0);          // pointer
	DDEBUG_GLERRORCHECK();

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo.ibuf);
	DDEBUG_GLERRORCHECK();

	GLuint size;
	switch (vbo.i_type)
	{
		case GL_UNSIGNED_SHORT:
			size = vbo.s.indices->size();
			break;
		case GL_UNSIGNED_INT:
			size = vbo.l.indices->size();
			break;
	}
	glDrawElements(vbo.mode,           // mode
	               size,               // size
	               vbo.i_type,         // type
	               0);                 // indices
	glDisableVertexAttribArray(va_vertices);

	return (DDEBUG_GLERRORCHECK() == 0);
}

/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 *
 * Given a set of vertices creates a reduced set of vertices, without 
 * duplicates, and a vector of indices that link the original set to the new
 * one.
 * 
 * There are 3 versions of indexVBO:
 * 	vertices, UVs and normals
 * 	vertices and normals
 * 	vertices
 *
 * TODO: this version do not reserve memory for vectors, maybe reserving as much
 * memory as the original ones is better or it's slower. 
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<Vec2>     &in_uvs,
                        std::vector<Vec3>     &in_normals,
                        std::vector<GLushort> &out_indices,
                        std::vector<Vec3>     &out_vertices,
                        std::vector<Vec2>     &out_uvs,
                        std::vector<Vec3>     &out_normals)
{
	std::map<PackedVertex,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		PackedVertex packed = {in_vertices[i], in_uvs[i], in_normals[i]};

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( packed, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			out_uvs     .push_back( in_uvs[i]);
			out_normals .push_back( in_normals[i]);
			GLushort newindex = (GLushort)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ packed ] = newindex;
#ifdef DEBUG_INDEX_VBO
			std::cout<<"newindex = "<<newindex<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}
	}
}

/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<Vec3>     &in_normals,
                        std::vector<GLushort> &out_indices,
                        std::vector<Vec3>     &out_vertices,
                        std::vector<Vec3>     &out_normals)
{
	std::map<PackedVertexNoUV,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		PackedVertexNoUV packed = {in_vertices[i], in_normals[i]};

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( packed, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			out_normals .push_back( in_normals[i]);
			GLushort newindex = (GLushort)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ packed ] = newindex;
#ifdef DEBUG_INDEX_VBO
			std::cout<<"newindex = "<<newindex<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}
	}
}

/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<GLushort> &out_indices,
                        std::vector<Vec3>     &out_vertices)
{
	std::map<Vec3,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		Vec3& v = in_vertices[i];

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( v, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			GLushort newindex = (GLushort)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ v ] = newindex;
#ifdef DEBUG_INDEX_VBO

			bool found = getSimilarVertexIndex( v, VertexToOutIndex, index);
			if (!found)
				assert(false);
			std::cout<<"newindex = "<<newindex<<" = "<<VertexToOutIndex[v]<<std::endl;
			v.print();
			std::cout<<std::endl;
#endif
		}
	}
}


/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 *
 * Given a set of vertices creates a reduced set of vertices, without 
 * duplicates, and a vector of indices that link the original set to the new
 * one.
 * 
 * There are 3 versions of indexVBO:
 * 	vertices, UVs and normals
 * 	vertices and normals
 * 	vertices
 *
 * TODO: this version do not reserve memory for vectors, maybe reserving as much
 * memory as the original ones is better or it's slower. 
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<Vec2>     &in_uvs,
                        std::vector<Vec3>     &in_normals,
                        std::vector<GLuint>   &out_indices,
                        std::vector<Vec3>     &out_vertices,
                        std::vector<Vec2>     &out_uvs,
                        std::vector<Vec3>     &out_normals)
{
	std::map<PackedVertex,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		PackedVertex packed = {in_vertices[i], in_uvs[i], in_normals[i]};

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( packed, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			out_uvs     .push_back( in_uvs[i]);
			out_normals .push_back( in_normals[i]);
			GLuint newindex = (GLuint)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ packed ] = newindex;
#ifdef DEBUG_INDEX_VBO
			std::cout<<"newindex = "<<newindex<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}
	}
}

/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<Vec3>     &in_normals,
                        std::vector<GLuint>   &out_indices,
                        std::vector<Vec3>     &out_vertices,
                        std::vector<Vec3>     &out_normals)
{
	std::map<PackedVertexNoUV,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		PackedVertexNoUV packed = {in_vertices[i], in_normals[i]};

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( packed, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			out_normals .push_back( in_normals[i]);
			GLuint newindex = (GLuint)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ packed ] = newindex;
#ifdef DEBUG_INDEX_VBO
			std::cout<<"newindex = "<<newindex<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}
	}
}

/**
 * code from http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-9-vbo-indexing/#Indexed_VBO_in_OpenGL
 */
void OGLTools::indexVBO(std::vector<Vec3>     &in_vertices,
                        std::vector<GLuint>   &out_indices,
                        std::vector<Vec3>     &out_vertices)
{
	std::map<Vec3,GLuint> VertexToOutIndex;

	// For each input vertex
	for ( unsigned int i=0; i<in_vertices.size(); i++ )
	{
		Vec3& v = in_vertices[i];

		// Try to find a similar vertex in out_XXXX
		GLuint u_index;
		bool found = getSimilarVertexIndex( v, VertexToOutIndex, u_index);

		if ( found )
		{ // A similar vertex is already in the VBO, use it instead !
			out_indices.push_back( u_index );
#ifdef DEBUG_INDEX_VBO
			std::cout<<"index = "<<u_index<<std::endl;
			in_vertices[i].print();
			std::cout<<std::endl;
#endif
		}else
		{ // If not, it needs to be added in the output data.
			out_vertices.push_back( in_vertices[i]);
			GLuint newindex = (GLuint)out_vertices.size() - 1;
			out_indices .push_back( newindex );
			VertexToOutIndex[ v ] = newindex;
#ifdef DEBUG_INDEX_VBO

			bool found = getSimilarVertexIndex( v, VertexToOutIndex, index);
			if (!found)
				assert(false);
			std::cout<<"newindex = "<<newindex<<" = "<<VertexToOutIndex[v]<<std::endl;
			v.print();
			std::cout<<std::endl;
#endif
		}
	}
}

bool OGLTools::getSimilarVertexIndex(
                        PackedVertex & packed,
                        std::map<PackedVertex,GLuint> &VertexToOutIndex,
                        GLuint &result)
{
	std::map<PackedVertex,GLuint>::iterator it = VertexToOutIndex.find(packed);
	if ( it == VertexToOutIndex.end() )
	{
		return false;
	}else
	{
		result = it->second;
		return true;
	}
}
bool OGLTools::getSimilarVertexIndex(
                    PackedVertexNoUV & packed,
                    std::map<PackedVertexNoUV,GLuint> &VertexToOutIndex,
                    GLuint &result)
{
	std::map<PackedVertexNoUV,GLuint>::iterator it = VertexToOutIndex.find(packed);
	if ( it == VertexToOutIndex.end() )
	{
		return false;
	}else
	{
		result = it->second;
		return true;
	}
}
bool OGLTools::getSimilarVertexIndex(
                                Vec3 & v,
                                std::map<Vec3,GLuint> &VertexToOutIndex,
                                GLuint &result)
{
	std::map<Vec3,GLuint>::iterator it = VertexToOutIndex.find(v);
	if ( it == VertexToOutIndex.end() )
	{
		return false;
	}else
	{
		result = it->second;
		return true;
	}
}
