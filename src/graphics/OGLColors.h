/** 
 * File:   OGLColors.h
 * Author: thegiallo
 * Directory: graphics
 *
 * Created on 15 December 2012
 * 
 */
#ifndef OGLCOLORS_H
#define	OGLCOLORS_H
#include <GL/glew.h>
#include <GL/gl.h>

class OGLColors
{
public:
	static GLfloat red[];
	static GLfloat green[];
	static GLfloat blue[];
	static GLfloat yellow[];
	static GLfloat cyan[];
	static GLfloat magenta[];
	static GLfloat black[];
	static GLfloat white[];
	static GLfloat gray[];
	static GLfloat violet[];
	static GLfloat brown[];
	static GLfloat purple[];
	static GLfloat orange[];
	static GLfloat gold[];
};

#endif /* OGLCOLORS_H */