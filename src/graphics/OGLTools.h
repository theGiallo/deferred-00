/**
 * Author: theGiallo
 * Directory: graphics
 **/
 
#ifndef OGLTOOLS_H
#define	OGLTOOLS_H

#include <GL/glew.h>
#include <GL/gl.h>
#include "../tgmath/Vec2.h"
#include "../tgmath/Vec3.h"
#include <vector>
#include <map>
#include <cstring>

class OGLTools
{
	using Vec3 = tgmath::f::Vec3;
	using Vec2 = tgmath::f::Vec2;
private:
	OGLTools();
public:
	static void translateVArray(GLfloat* vArray,
	                            unsigned int count,
	                            const Vec2& t);
	static void rotateVArrayDeg(GLfloat* vArray,
	                            unsigned int count,
	                            float rot);
	static void scaleVArray(GLfloat* vArray,
	                        unsigned int count,
	                        const Vec2& s);

	struct Program
	{
		GLuint program;
		GLuint va_vertices,
		       va_uvs,
		       va_normals;
		GLuint u_M, u_V, u_P, u_MVP,
		       u_light_pos, u_light_power,
		       u_texture;
		GLuint vs,
		       gs,
		       fs;
	};

	static Program * current_program;

	static bool createProgram(Program & program,
		                       GLuint vs_count,
		                       const GLchar ** vs_strings,
		                       const GLint * vs_lengths,
		                       GLuint fs_count,
		                       const GLchar ** fs_strings,
		                       const GLint * fs_lengths,
		                       GLuint gs_count = 0,
		                       const GLchar ** gs_strings = nullptr,
		                       const GLint * gs_lengths = nullptr);

	union VBO_iv
	{
		struct S
		{
			std::vector<tgmath::f::Vec3> * vertices;
			std::vector<GLushort> * indices;
			GLuint vbuf,
			       ibuf;
			GLenum mode; // GL drawing mode
			             // GL_POINTS,
			             // GL_LINE_STRIP,
			             // GL_LINE_LOOP,
			             // GL_LINES,
			             // GL_LINE_STRIP_ADJACENCY,
			             // GL_LINES_ADJACENCY,
			             // GL_TRIANGLE_STRIP,
			             // GL_TRIANGLE_FAN,
			             // GL_TRIANGLES,
			             // GL_TRIANGLE_STRIP_ADJACENCY and
			             // GL_TRIANGLES_ADJACENCY
			GLenum i_type;
			size_t i_size;
		} s;

		struct L
		{
			std::vector<tgmath::f::Vec3> * vertices;
			std::vector<GLuint> * indices;
			GLuint vbuf,
			       ibuf;
			GLenum mode;
			GLenum i_type;
			size_t i_size;
		} l;

		struct
		{
			std::vector<tgmath::f::Vec3> * vertices;
			void * indices;
			GLuint vbuf,
			       ibuf;
			GLenum mode;
			GLenum i_type;
			size_t i_size;
		};
	};

	// struct VBO_ivn
	// {
	// 	std::vector<tgmath::f::Vec3> vertices;
	// 	std::vector<tgmath::f::Vec3> normals;
	// 	std::vector<> indices;
	// };

	static void destroyVBO(VBO_iv & vbo);
	static void destroyProgramAndShaders(Program & program);
	static void createVBO_siv(VBO_iv & vbo);
	static void createVBO_liv(VBO_iv & vbo);
	static void deleteVBO_iv(VBO_iv & vbo);
	static void deleteVBO_siv(VBO_iv & vbo);
	static void deleteVBO_liv(VBO_iv & vbo);
	static bool initVBO(VBO_iv & vbo);
	static bool drawVBO(const VBO_iv & vbo);

	// GLushort
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<Vec2>      &in_uvs,
	                     std::vector<Vec3>      &in_normals,
	                     std::vector<GLushort>  &out_indices,
	                     std::vector<Vec3>      &out_vertices,
	                     std::vector<Vec2>      &out_uvs,
	                     std::vector<Vec3>      &out_normals);
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<Vec3>      &in_normals,
	                     std::vector<GLushort>  &out_indices,
	                     std::vector<Vec3>      &out_vertices,
	                     std::vector<Vec3>      &out_normals);
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<GLushort>  &out_indices,
	                     std::vector<Vec3>      &out_vertices);

	// GLuint
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<Vec2>      &in_uvs,
	                     std::vector<Vec3>      &in_normals,
	                     std::vector<GLuint>  &out_indices,
	                     std::vector<Vec3>      &out_vertices,
	                     std::vector<Vec2>      &out_uvs,
	                     std::vector<Vec3>      &out_normals);
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<Vec3>      &in_normals,
	                     std::vector<GLuint>  &out_indices,
	                     std::vector<Vec3>      &out_vertices,
	                     std::vector<Vec3>      &out_normals);
	static void indexVBO(std::vector<Vec3>      &in_vertices,
	                     std::vector<GLuint>  &out_indices,
	                     std::vector<Vec3>      &out_vertices);

private:

	struct PackedVertex
	{
		Vec3 position;
		Vec2 uv;
		Vec3 normal;
		bool operator<(const PackedVertex that) const{
			return memcmp((void*)this, (void*)&that, sizeof(PackedVertex))>0;
		};
	};
	struct PackedVertexNoUV
	{
		Vec3 position;
		Vec3 normal;
		bool operator<(const PackedVertexNoUV that) const{
			return memcmp((void*)this, (void*)&that, sizeof(PackedVertexNoUV))>0;
		};
	};

	static bool getSimilarVertexIndex(
	                 PackedVertex & packed,
	                 std::map<PackedVertex, GLuint> & VertexToOutIndex,
	                 GLuint & result);
	static bool getSimilarVertexIndex(
	                 PackedVertexNoUV & packed,
	                 std::map<PackedVertexNoUV,GLuint> &VertexToOutIndex,
	                 GLuint &result);
	static bool getSimilarVertexIndex(
	                 Vec3 & v,
	                 std::map<Vec3,GLuint> &VertexToOutIndex,
	                 GLuint &result);
};
#endif	/* OGLTOOLS_H */
