#include "OGLColors.h"

GLfloat OGLColors::red[]     = {1.0f, 0.0f, 0.0f, 1.0f};
GLfloat OGLColors::green[]   = {0.0f, 1.0f, 0.0f, 1.0f};
GLfloat OGLColors::blue[]    = {0.0f, 0.0f, 1.0f, 1.0f};
GLfloat OGLColors::yellow[]  = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat OGLColors::cyan[]    = {.0f, 1.0f, 1.0f, 1.0f};
GLfloat OGLColors::magenta[] = {1.0f, .0f, 1.0f, 1.0f};
GLfloat OGLColors::black[]   = {.0f, .0f, .0f, 1.0f};
GLfloat OGLColors::white[]   = {1.0f, 1.0f, 1.0f, 1.0f};
GLfloat OGLColors::gray[]    = {.5f, .5f, .5f, 1.0f};
GLfloat OGLColors::violet[]  = {.541176f, .168627f, .886274f, 1.0f};
GLfloat OGLColors::brown[]   = {.647f, .17647f, .17647f, 1.0f};
GLfloat OGLColors::purple[]  = {.5f, .0f, .5, 1.0f};
GLfloat OGLColors::orange[]  = {1.0f, 0.27058f, .0f, 1.0f};
GLfloat OGLColors::gold[]    = {1.0f, 0.84314f, .0f, 1.0f};