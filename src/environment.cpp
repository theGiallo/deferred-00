#include "environment.h"

namespace Environment
{
	SDL_Window * window = nullptr;
	SDL_GLContext glcontext;

	float zoom = 1.0f;

	std::vector<SDL_GameController *> game_controllers;
	std::map<int,int> gc_which; // NOTE: gc_which[joystick_instanceID] = joystick_number;
	tgmath::i::Vec2 left_stick[MAX_CONTROLLERS],
	                right_stick[MAX_CONTROLLERS];
	int left_trigger[MAX_CONTROLLERS],
	    right_trigger[MAX_CONTROLLERS];

	static bool initialized = false;

	void init()
	{
		if (!initialized)
		{
			initialized = true;

			for (int i = 0; i < MAX_CONTROLLERS; ++i)
			{
				left_trigger[i] =
				right_trigger[i] = 0;
				left_stick[i] =
				right_stick[i] = {0,0};
			}
		}
	}

	void update()
	{
		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);

		// TODO(theGiallo, 2015/08/17): do something to calculate zoom, for GUI and fonts
		// zoom = ww/(float)world_width;
	}

	int gameControllerIDFromJoyopadID(int joypad_ID)
	{
		// NOTE: if it's not there the value is unknown
		// TODO(theGiallo, 2015/02/28): maybe we need to manage the above case
		return gc_which[joypad_ID];
	}
}