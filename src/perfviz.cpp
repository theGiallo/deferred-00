#include "perfviz.h"
#include "dbg.h"
#include "tgmath/MathTools.h"
#include "enginegears.h"

namespace tg
{
	// Methods
	//---
	PerfViz::PerfViz()
	{
		reset();
	}
	PerfViz::~PerfViz()
	{
		if (_owns_buffer && _buffer)
			delete _buffer;
	}
	void
	PerfViz::init()
	{
		assert(_buffer_size >= 0);
		if (!tgmath::i::isPowerOf2(_buffer_size))
		{
			_buffer_size = (int) tgmath::roundToNextPow2((uint32_t)_buffer_size);
		}
		assert(tgmath::i::isPowerOf2(_buffer_size));
		if (_buffer_size && !_buffer)
		{
			_owns_buffer = true;
			_buffer = new Time[_buffer_size];
			assert(_buffer);
		}
	}
	void
	PerfViz::reset()
	{
		_buffer = nullptr;
		_buffer_size = 0;
		_tail_id = 0;
		_owns_buffer = false;

		width = 128;
		height = 32;
		center = {width/2, height/2};
		samples_visualized = _buffer_size;
		max_value = 1.0/10.0;
		graph_color = {0xFF, 0x00, 0x00, 0xFF};
		border_color = {0xFF, 0xFF, 0xFF, 0x80};
		style = LINESTRIP;

		#if DEBUG
			rendered = true;
		#else
			rendered = false;
		#endif
	}

	void
	PerfViz::render()
	{
		if (!rendered)
			return;
		tgmath::i::Vec2 origin = center - tgmath::i::Vec2{width/2, height/2};

		for (int i = 1; i < 10; ++i)
		{
			tgmath::i::Vec2 s = origin + tgmath::i::Vec2{0,(int)(i*height/10.0)};
			renderLine(s,
			           s+tgmath::i::Vec2{width,0},
			           border_color);
		}
		{
			tgmath::i::Vec2 s = origin + tgmath::i::Vec2{0,(int)(height/max_value/tg::EXPECTED_FPS)};
			renderLine(s,
			           s+tgmath::i::Vec2{width,0},
			           {0,255,0,255});
		}
		{
			tgmath::i::Vec2 s = origin + tgmath::i::Vec2{0,(int)(height/max_value/tg::MAX_FPS)};
			renderLine(s,
			           s+tgmath::i::Vec2{width,0},
			           {0,0,255,255});
		}
		#if DEBUG
		{
			tgmath::i::Vec2 s = origin + tgmath::i::Vec2{0,(int)(height/max_value/tg::MIN_FPS)};
			renderLine(s,
			           s+tgmath::i::Vec2{width,0},
			           {255,0,0,255});
		}
		#endif

		switch (style)
		{
		case VLINES:
			assert(!"VLINES not yet implemented!");
			break;
		case AREA:
			assert(!"AREA not yet implemented!");
			break;
		case LINESTRIP:
		{
			#if 1
			// assert(!"LINESTRIP not yet implemented!");
			int dx = width / samples_visualized;
			int start = 0;
			if (_tail_id > samples_visualized)
			{
				start = _tail_id - samples_visualized;
			}
			for (int i = start; i < _tail_id-1; ++i)
			{
				int sid = i-start;
				renderLine(origin+tgmath::i::Vec2{sid*dx,(int)(height*_buffer[i]/max_value)},
				           origin+tgmath::i::Vec2{(sid+1)*dx,(int)(height*_buffer[i+1]/max_value)},
				           graph_color);
			}
			#endif
			break;
		}
		default:
			break;
		}
	}
	void
	PerfViz::addSample(Time sample)
	{
		_buffer[_tail_id++] = sample;
		if (_tail_id == _buffer_size)
		{
			FILE * f = fopen("perfviz.times", "a+");

			if (f)
			{
				for (int i = 0; i < _tail_id; ++i)
				{
					fprintf(f, "%d %lf\n", i, _buffer[i]);
				}

				fclose(f);
			} else
			{
				perror("creating PerfViz dump file");
				errno = 0;
			}
		}
		_tail_id &= _buffer_size-1; // NOTE(theGiallo): works because size is power of 2
		// TODO(theGiallo, 2015/04/11): make it circular
		// TODO(theGiallo, 2015/04/11): make it double buffered and write to file in a separate thread
	}

	void
	PerfViz::centerHorizontally()
	{
		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		center.x = ww/2;
	}
	void
	PerfViz::centerVertically()
	{
		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		center.y = wh/2;
	}
	void
	PerfViz::distFromBottom(int dist)
	{
		center.y = height/2 + dist;
	}
	void
	PerfViz::distFromTop(int dist)
	{
		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		center.y = wh - height/2 - dist;
	}
	void
	PerfViz::distFromLeft(int dist)
	{
		center.x = dist;
	}
	void
	PerfViz::distFromRight(int dist)
	{
		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		center.x = ww - width/2;
	}
}