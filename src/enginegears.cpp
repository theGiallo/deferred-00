#include "enginegears.h"
#ifdef ANDROID
	#include <SDL.h>
#else
	#include <SDL2/SDL.h>
#endif

namespace tg
{
	Time time_accumulator,
	      prepause_time_accumulator;
	bool running;
	bool paused;
	bool has_to_start;

	float fps;
	float ms_per_frame;

	double
	getTimeInSec()
	{
		static double coeff;
		static bool isInitialized;
		if (!isInitialized) {
			isInitialized = true;
			Uint64 freq = SDL_GetPerformanceFrequency();
			coeff = 1.0 / (double)freq;
		}
		Uint64 val = SDL_GetPerformanceCounter();

		return (double)val * coeff;
	}

	void
	initEngine()
	{
		running = true;
		paused = false;
		has_to_start = true;
		// time_accumulator = 0.0;
		prepause_time_accumulator = 0.0;
	}

	void
	pauseSwitch()
	{
		if (paused)
		{
			time_accumulator = prepause_time_accumulator;
		} else
		{
			prepause_time_accumulator = time_accumulator;
		}
		paused = !paused;
	}
}