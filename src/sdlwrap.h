#pragma once

#include "environment.h"
#ifdef ANDROID
	#include <SDL.h>
#else
	#include <SDL2/SDL.h>
#endif

#include "tgmath/Vec2.h"

#ifdef DEBUG
#define DCHECKSDLERROR(ret) if (ret != 0)\
	{\
		const char *error = SDL_GetError();\
		if (*error != '\0')\
		{\
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL error: %s at line #%d of file %s", error, __LINE__, __FILE__);\
			SDL_ClearError();\
		}\
	}
#else
#define DCHECKSDLERROR(ret)
#endif

using ColorRGBA = struct ColorRGBA_st
{
	Uint8 r,g,b,a;
	operator SDL_Color();
};

void
getRenderColor(SDL_Renderer* renderer, ColorRGBA& color);
void
setRenderColor(SDL_Renderer* renderer, const ColorRGBA& color);
void
setRenderColor(const ColorRGBA& color);

using SDLWrap_Texture = struct SDLWrap_Texture_st
{
	SDL_Texture* texture;
	SDL_Rect size;
};
using TextureAtlas = struct TextureAtlas_st
{
	SDLWrap_Texture texture;
	tgmath::i::Vec2 frame_size;
	tgmath::i::Vec2 atlas_size_in_frames;
};

bool
renderTexturedRect(const SDLWrap_Texture& texture, const SDL_Rect& rect, const ColorRGBA& color = {255,255,255,255});
bool
renderRectBorder(const SDL_Rect& rect, const ColorRGBA& color);
bool
renderRectPlain(const SDL_Rect& rect, const ColorRGBA& color);
bool
renderLine(const tgmath::i::Vec2 start, const tgmath::i::Vec2 end, const ColorRGBA& color);
bool
renderPoint(const tgmath::i::Vec2& p, const ColorRGBA& color);

void
loadTexture(const std::string & file_path,
            SDLWrap_Texture & texture);
void
renderTexture(const SDLWrap_Texture& texture,
              const tgmath::i::Vec2 & pos,
              float rot=0.0f,
              float scale=1.0f,
              Uint8 flip=SDL_FLIP_NONE);
void
renderTexture(const SDLWrap_Texture& texture,
              const tgmath::i::Vec2 & pos,
              float rot,
              float scale,
              const ColorRGBA & color,
              Uint8 flip = SDL_FLIP_NONE);
void
renderTextureFromAtlas(const SDLWrap_Texture& texture_atlas,
                       const tgmath::i::Vec2 & frame_size,
                       const tgmath::i::Vec2 & atlas_size_in_frames,
                       int frame_ID,
                       const tgmath::i::Vec2 & pos,
                       float rot,
                       float scale,
                       const ColorRGBA & color,
                       Uint8 flip,
                       const tgmath::i::Vec2 & center = {});

void
renderCircle(const tgmath::i::Vec2 & center,
             float radius,
             const ColorRGBA & color,
             int sides=0);

#ifdef ANDROID

const Sint16 SDL_AXIS_TRIGGER_ZERO = -32767;
const Sint16 SDL_AXIS_TRIGGER_MAX  = 32767;
const Uint8 SDL_JOYAXIS_L_X = 0;
const Uint8 SDL_JOYAXIS_L_Y = 1;
const Uint8 SDL_JOYAXIS_L_T = 4; // and 7
const Uint8 SDL_JOYAXIS_R_X = 2;
const Uint8 SDL_JOYAXIS_R_Y = 3;
const Uint8 SDL_JOYAXIS_R_T = 5; // and 7

const Uint8 SDL_JOY_BUTTON_A = 0;
const Uint8 SDL_JOY_BUTTON_B = 1;
const Uint8 SDL_JOY_BUTTON_X = 2;
const Uint8 SDL_JOY_BUTTON_Y = 3;
const Uint8 SDL_JOY_BUTTON_START = 6;
const Uint8 SDL_JOY_BUTTON_LS = 7;
const Uint8 SDL_JOY_BUTTON_RS = 8;
const Uint8 SDL_JOY_BUTTON_LB = 9;
const Uint8 SDL_JOY_BUTTON_RB = 10;
const Uint8 SDL_JOY_BUTTON_DU = 11;
const Uint8 SDL_JOY_BUTTON_DD = 12;
const Uint8 SDL_JOY_BUTTON_DL = 13;
const Uint8 SDL_JOY_BUTTON_DR = 14;

#else

const Sint16 SDL_AXIS_TRIGGER_ZERO = -32768;
const Sint16 SDL_AXIS_TRIGGER_MAX  = 32767;
const Uint8 SDL_JOYAXIS_L_X = 0;
const Uint8 SDL_JOYAXIS_L_Y = 1;
const Uint8 SDL_JOYAXIS_L_T = 2;
const Uint8 SDL_JOYAXIS_R_X = 3;
const Uint8 SDL_JOYAXIS_R_Y = 4;
const Uint8 SDL_JOYAXIS_R_T = 5;

const Uint8 SDL_JOY_BUTTON_A = 0;
const Uint8 SDL_JOY_BUTTON_B = 1;
const Uint8 SDL_JOY_BUTTON_X = 2;
const Uint8 SDL_JOY_BUTTON_Y = 3;
const Uint8 SDL_JOY_BUTTON_LB = 4;
const Uint8 SDL_JOY_BUTTON_RB = 5;

#endif

const int JOYAXIS_LOWLIMIT = 8000;

/**
 * Creates a SDL_Rect from a center-ref rectangle
 **/
SDL_Rect SDLRect(int center_x, int center_y, int width, int hwight);