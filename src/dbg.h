#pragma once

#include <cstdio>
#include <cassert>


#define CNSL_RESET "\e[00m"
#define CNSL_BLACK "\e[0;30m"
#define CNSL_RED "\e[0;31m"
#define CNSL_GREEN "\e[0;32m"
#define CNSL_YELLOW "\e[0;33m"
#define CNSL_BLUE "\e[0;34m"
#define CNSL_PURPLE "\e[0;35m"
#define CNSL_CYAN "\e[0;36m"
#define CNSL_WHITE "\e[0;37m"

// TODO(theGiallo, 2015/08/01): better define empty defines

#if DEBUG_VERBOSE
	#if defined(DEBUG) && !DEBUG
		#undef DEBUG
	#endif
	#ifndef DEBUG
		#define DEBUG 1
	#endif
#endif

#ifndef APP_NAME
	#define APP_NAME "CLOG"
#endif

#ifdef ANDROID
	#include <android/log.h>
	#define printf(...) __android_log_print(ANDROID_LOG_DEBUG, APP_NAME, __VA_ARGS__)
#endif

#define log_info(format, ...) printf(format "\n", ##__VA_ARGS__)

#define dbg_here(s) log_dbg(CNSL_YELLOW s CNSL_PURPLE "@" CNSL_CYAN __FILE__ CNSL_GREEN ":" CNSL_WHITE "%d\n" CNSL_RESET, __LINE__)
#define ILLEGAL_PATH() log_dbg(CNSL_RED "ILLEGAL PATH" CNSL_PURPLE "@" CNSL_CYAN __FILE__ CNSL_GREEN ":" CNSL_WHITE "%d \n" CNSL_RESET, __LINE__); assert(false); ++*(int*)NULL;

#if DEBUG_VERBOSE
	#define log_dbg_v(...) log_info(__VA_ARGS__)
#else
	#define log_dbg_v(...)
#endif
#if DEBUG
	#define log_dbg(...) log_info(__VA_ARGS__)
#else
	#define log_dbg(...)
#endif

#ifdef ANDROID
	#define log_err(...) __android_log_print(ANDROID_LOG_ERROR, APP_NAME, __VA_ARGS__)
#else
	#define log_err(format, ...) fprintf(stderr, CNSL_RED format CNSL_RESET "\n", ##__VA_ARGS__)
#endif

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)
#define HERE() __FILE__ ":" TOSTRING(__LINE__)

#define PERROR_CLEAN(...) do{perror(__VA_ARGS__); errno=0;}while(false)