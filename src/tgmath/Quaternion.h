/* 
 * File:   Quaternion.h
 * Author: thegiallo
 *
 *  code taken from http://gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
 *
 * Created on 29 gennaio 2011, 15.20
 */

// #ifndef QUATERNION_H
// #define	QUATERNION_H
#pragma once

namespace tgmath
{
	namespace f
	{
		class Quaternion;
		class Vec3;
		class Matrix4;

		class Quaternion
		{
		protected:
			float w,x,y,z;

		public:
			Quaternion();
			Quaternion(float x, float y, float z, float w);
			~Quaternion();

			void normalize();
			Quaternion getConjugate() const;
			const Quaternion operator* (const Quaternion &rq) const;
			const Vec3 operator* (const Vec3& vec) const;
			void FromAxis(const Vec3& v, float angle);
			void FromEuler(float pitch, float yaw, float roll);
			Matrix4 getMatrix() const;
			void getAxisAngle(Vec3& axis, float& angle) const;
		}; // class
	}; // namespace
}; // namespace

#include "Vec3.h"
#include "Matrix4.h"

// #endif	/* QUATERNION_H */