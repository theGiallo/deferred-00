#include "MathTools.h"

namespace tgmath
{
	namespace f
	{
		float (*sign)(float) = tgmath::sign<float>;
		void (*clampAngleDeg)(float&) = tgmath::clampAngleDeg<float>;
		float distFromLine(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t)
		{
			Vec2 v0v1 = v1-v0;
			Vec2 v0X = X-v0;
			float t = (v0X*v0v1)/(v0v1*v0v1);
			if (out_P_on_seg!=nullptr)
				*out_P_on_seg = v0+v0v1*t;
			if (out_t!=nullptr)
				*out_t = t;
			return (X-*out_P_on_seg).getModule();
		}
		float distFromSegment(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t)
		{
			float t;
			float d = distFromLine(X, v0, v1, out_P_on_seg, &t);
			if (t<0)
			{
				t=0;
				if (out_P_on_seg!=nullptr)
				{
					*out_P_on_seg = v0;
				}
				d = (X-v0).getModule();
			}
			if (t>1)
			{
				t=1;
				if (out_P_on_seg!=nullptr)
				{
					*out_P_on_seg = v1;
				}
				d = (X-v1).getModule();
			}
			if (out_t!=nullptr)
			{
				*out_t=t;
			}
			return d;
		}
	};
	namespace d
	{
		double (*sign)(double) = tgmath::sign<double>;
		void (*clampAngleDeg)(double&) = tgmath::clampAngleDeg<double>;
	};
	namespace i
	{
		bool isPowerOf2(unsigned int x)
		{
			return x && !(x & (x - 1));
		}
	};
	uint32_t roundToNextPow2(uint32_t v)
	{
		v--;
		v |= v >> 1;
		v |= v >> 2;
		v |= v >> 4;
		v |= v >> 8;
		v |= v >> 16;
		v++;
		return v;
	}
};