#include "Vec3.h"
#include "MathTools.h"

#include <cmath>
#include <iostream>
#include <cassert>


namespace tgmath
{
	namespace f // --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
	{
		Vec3::Vec3()
		{
			x = y = z = 0;
		}
		Vec3::Vec3(float x, float y, float z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}
		Vec3::Vec3(const tgmath::i::Vec3& v)
		{
			this->x = v.x;
			this->y = v.y;
			this->z = v.z;
		}
		Vec3::operator tgmath::i::Vec3()
		{
			return tgmath::i::Vec3(x,y,z);
		}
		float Vec3::getModule(void) const
		{
			return sqrt(x*x+y*y+z*z);
		}
		float Vec3::getSin(const Vec3& v) const
		{
			return (this->getNormalized()^v.getNormalized()).getModule();
		}
		float Vec3::getCos(const Vec3& v) const
		{
			return this->getNormalized()*v.getNormalized();
		}
		void Vec3::normalize(void)
		{
			float l = this->getModule();
			if ( l != 0 )
			{
				*this /= l;
			}
		}
		Vec3 Vec3::getNormalized(void) const
		{
			Vec3 tmp = getClone();
			tmp.normalize();
			return tmp;
		}
		void Vec3::add(const Vec3& v)
		{
			x+=v.x;
			y+=v.y;
			z+=v.z;
		}
		const Vec3 Vec3::operator +(const Vec3& v) const
		{
			Vec3 tmp = this->getClone();
			tmp.add(v);
			return tmp;
		}
		Vec3& Vec3::operator +=(const Vec3& v)
		{
			this->add(v);
			return *this;
		}
		void Vec3::sub(const Vec3& v)
		{
			x-=v.x;
			y-=v.y;
			z-=v.z;
		}
		const Vec3 Vec3::operator -(void) const
		{
			return *this*-1.0f;
		}
		const Vec3 Vec3::operator -(const Vec3& v) const
		{
			Vec3 tmp = this->getClone();
			tmp.sub(v);
			return tmp;
		}
		Vec3& Vec3::operator -=(const Vec3& v)
		{
			this->sub(v);
			return *this;
		}
		void Vec3::add(uint8_t i, float f)
		{
			switch(i%3)
			{
				case X_COMPONENT:
					x += f;
					break;
				case Y_COMPONENT:
					y += f;
					break;
				case Z_COMPONENT:
					z += f;
					break;
			}
		}
		void Vec3::add(float k)
		{
			x+=k;
			y+=k;
			z+=k;
		}
		const Vec3 Vec3::operator +(float k) const
		{
			Vec3 tmp = this->getClone();
			tmp.add(k);
			return tmp;
		}
		Vec3& Vec3::operator +=(float k)
		{
			this->add(k);
			return *this;
		}
		void Vec3::sub(float k)
		{
			x-=k;
			y-=k;
			z-=k;
		}
		const Vec3 Vec3::operator -(float k) const
		{
			Vec3 tmp = this->getClone();
			tmp.sub(k);
			return tmp;
		}
		Vec3& Vec3::operator -=(float k)
		{
			this->sub(k);
			return *this;
		}
		void Vec3::mult(uint8_t i, float f)
		{
			switch(i%3)
			{
				case X_COMPONENT:
					x *= f;
					break;
				case Y_COMPONENT:
					y *= f;
					break;
				case Z_COMPONENT:
					z *= f;
					break;
			}
		}
		void Vec3::mult(float k)
		{
			x*=k;
			y*=k;
			z*=k;
		}
		const Vec3 Vec3::operator *(float k) const
		{
			Vec3 tmp = this->getClone();
			tmp.mult(k);
			return tmp;
		}
		Vec3& Vec3::operator *=(float k)
		{
			this->mult(k);
			return *this;
		}
		void Vec3::div(uint8_t i, float f)
		{
			switch(i%3)
			{
				case X_COMPONENT:
					x /= f;
					break;
				case Y_COMPONENT:
					y /= f;
					break;
				case Z_COMPONENT:
					z /= f;
					break;
			}
		}
		void Vec3::div(float k)
		{
			if (k==0)
				return;
			x/=k;
			y/=k;
			z/=k;
		}
		const Vec3 Vec3::operator/(float k) const
		{
			Vec3 tmp = this->getClone();
			tmp.div(k);
			return tmp;
		}
		Vec3& Vec3::operator/=(float k)
		{
			this->div(k);
			return *this;
		}
		float Vec3::getScalar(const Vec3& v) const
		{
			return (x*v.x+y*v.y+z*v.z);
		}
		const float Vec3::operator *(const Vec3& v) const
		{
			return this->getScalar(v);
		}
		Vec3 Vec3::getVectorial(const Vec3& v) const
		{
			return Vec3( y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x );
		}
		const Vec3 Vec3::operator ^(const Vec3& v) const
		{
			return this->getVectorial(v);
		}
		Vec3& Vec3::operator ^=(const Vec3& v)
		{
			*this = this->getVectorial(v);
			return *this;
		}
		bool Vec3::isEqual(const Vec3& v)
		{
			return x==v.x&&
						y==v.y&&
						z==v.z;
		}
		bool Vec3::operator ==(const Vec3& v)
		{
			return this->isEqual(v);
		}
		bool Vec3::operator !=(const Vec3& v)
		{
			return !(this->isEqual(v));
		}
		void Vec3::clone(const Vec3& v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}
		Vec3 Vec3::getClone(void) const
		{
			return Vec3(x,y,z);
		}
		Vec3& Vec3::operator =(const Vec3& v)
		{
			if (this!=&v)
			{
				this->clone(v);
			}
			return *this;
		}
		float Vec3::get(uint8_t i)
		{
			switch(i%3)
			{
				case X_COMPONENT:
					return x;
				case Y_COMPONENT:
					return y;
				case Z_COMPONENT:
					return z;
				default:
					assert(false);
					return 0.0f;
			}
		}
		float  Vec3::getX(void) const
		{
			return x;
		}
		float  Vec3::getY(void) const
		{
			return y;
		}
		float  Vec3::getZ(void) const
		{
			return z;
		}
		float const & Vec3::operator [](unsigned int i) const
		{
		//	std::cout<<"const vec3::operator []"<<std::endl;
			assert (i<3);
			return (&x)[i];
		}
		float & Vec3::operator [](unsigned int i)
		{
		//	std::cout<<"vec3::operator []"<<std::endl;
			assert (i<3);
			return (&x)[i];
		}
		Vec3::operator const float*() const
		{
		//	std::cout<<"vec3::operator const float*"<<std::endl;
			return &x;
		}
		Vec3::operator float*()
		{
		//	std::cout<<"vec3::operator const float*"<<std::endl;
			return &x;
		}
		void Vec3::set(uint8_t i, float f)
		{
			switch(i%3)
			{
				case X_COMPONENT:
					x = f;
					break;
				case Y_COMPONENT:
					y = f;
					break;
				case Z_COMPONENT:
					z = f;
					break;
				default:
					assert(false);
					break;
			}
		}
		void  Vec3::setX(float x)
		{
			this->x = x;
		}
		void  Vec3::setY(float y)
		{
			this->y = y;
		}
		void  Vec3::setZ(float z)
		{
			this->z = z;
		}
		void Vec3::print() const
		{
			std::cout<<"("<<x<<", "<<y<<", "<<z<<")"<<std::endl;
		}

		void Vec3::rotate(const Quaternion& q, const Vec3& center)
		{;
			*this = q*(*this - center) + center;
		}
		Vec3 Vec3::getRotated(const Quaternion& q, const Vec3& center) const
		{
			Vec3 tmp = *this;
			tmp.rotate(q,center);
			return tmp;
		}
		void Vec3::scale(const Vec3& s, const Vec3& center)
		{
			x = (x-center.x)*s.x+center.x;
			y = (y-center.y)*s.y+center.y;
			z = (z-center.z)*s.z+center.z;
		}
		Vec3 Vec3::getScaled(const Vec3& s, const Vec3& center) const
		{
			Vec3 tmp = *this;
			tmp.scale(s,center);
			return tmp;
		}
	}; // namespace
	namespace i // --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
	{
		Vec3::Vec3()
		{
			x=y=z=0;
		}
		Vec3::Vec3(int x, int y, int z)
		{
			this->x=x;
			this->y=y;
			this->z=z;
		}
		Vec3::Vec3(int* xyz)
		{
			this->x=xyz[X_COMPONENT];
			this->y=xyz[Y_COMPONENT];
			this->z=xyz[Z_COMPONENT];
		}
		Vec3::Vec3(const tgmath::f::Vec3& v)
		{
			this->x = v.x;
			this->y = v.y;
			this->z = v.z;
		}

		Vec3::operator tgmath::f::Vec3()
		{
			return tgmath::f::Vec3(x,y,z);
		}
		const Vec3 Vec3::operator +(const Vec3& v) const
		{
			return Vec3(x+v.x,y+v.y,z+v.z);
		}
		//Vec3 Vec3::operator +(const Vec3& v)
		//{
		//	return Vec3(x+v.x,y+v.y,z+v.z);
		//}
		Vec3& Vec3::operator +=(const Vec3& v)
		{
			x+=v.x;
			y+=v.y;
			z+=v.z;
			return *this;
		}
		const Vec3 Vec3::operator -(const Vec3& v) const
		{
			return Vec3(x-v.x,y-v.y,z-v.z);
		}
		//Vec3 Vec3::operator -(const Vec3& v)
		//{
		//	return Vec3(x-v.x,y-v.y,z-v.z);
		//}
		Vec3& Vec3::operator -=(const Vec3& v)
		{
			x-=v.x;
			y-=v.y;
			z-=v.z;
			return *this;
		}
		bool Vec3::operator ==(const Vec3& v)
		{
			return x==v.x&&y==v.y&&z==v.z;
		}
		bool Vec3::operator !=(const Vec3& v)
		{
			return x!=v.x||y!=v.y||z!=v.z;
		}
		void Vec3::print() const
		{
			std::cout<<"("<<x<<", "<<y<<", "<<z<<")";
		}
	}; // namespace
}; // namespace