#include "random.h"

namespace tgmath
{

	// NOTE: uint64_t seeds[2]; The state must be seeded so that it is not everywhere zero
	uint64_t xorshift128plus(uint64_t seeds[2])
	{
		uint64_t x = seeds[0];
		uint64_t const y = seeds[1];
		seeds[0] = y;
		x ^= x << 23; // a
		x ^= x >> 17; // b
		x ^= y ^ (y >> 26); // c
		seeds[1] = x;
		return x + y;
	}

	float normalAVGRNDXorShiftPlus(uint64_t seeds[2], int rng_count=4)
	{
		float r = 0.0f;
		for (int i=0; i!=rng_count; ++i)
		{
			// r += dis(gen);
			uint64_t R = xorshift128plus(seeds);
			r += (float)(R / (float)0xFFFFFFFFFFFFFFFFL);
		}
		r /= (float)rng_count;
		return r;
	}
}