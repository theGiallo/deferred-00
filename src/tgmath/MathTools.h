#pragma once
// #ifndef MATHTOOLS_H
	// #define MATHTOOLS_H

#if 0 and defined ANDROID
	// #include <math.h>
	#include <cmath>
	#define ROUND round
	#define MODF modf
	#define ABS abs
	#define MAX max
#else
	#include <cmath>
	#define ROUND std::round
	#define MODF std::modf
	#define ABS std::abs
	#define MAX std::max
#endif

#include "MathConsts.h"
#include "Vec2.h"

namespace tgmath
{
	template <typename T>
	inline T min(T a, T b) {return ((a)<(b)?(a):(b));};
	template <typename T>
	inline T max(T a, T b) {return ((a)<(b)?(a):(b));};
	template <typename T>
	inline T sign(T a) {return (((a) > 0) - ((a) < 0));};
	template <typename T>
	void clampAngleDeg(T &a) {int A = (a/360);  A*= 360; a = a - A; if (a<0) a+=360;}

	namespace f
	{
		inline float DEG2RAD(float a)
		{
			return a*PI/180.0f;
		};
		inline float RAD2DEG(float a)
		{
			return a*180.0f/PI;
		};

		extern float (*sign)(float);
		extern void (*clampAngleDeg)(float&);

		/**
		 * Returns the distance between point X and the segment v0-v1.
		 * If the ortogonal projection of X on the line v0v1 is outside from the
		 * segment returns the distance from the nearest of v0 and v1.
		 *
		 * P = v0 + t*(v1-v0);
		 *
		 * If the out parameters are nullptr they are not assigned.
		 **/
		float distFromSegment(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t);

		/**
		 * Returns the distance between point X and the line passing through v0 
		 * and v1.
		 *
		 * P = v0 + t*(v1-v0);
		 * 
		 * If the out parameters are nullptr they are not assigned.
		 **/
		float distFromLine(const Vec2& X, const Vec2& v0, const Vec2& v1, Vec2* out_P_on_seg, float* out_t);
	};
	namespace d
	{
		inline double DEG2RAD(double a)
		{
			return a*PI/180.0f;
		};
		inline double RAD2DEG(double a)
		{
			return a*180.0f/PI;
		};

		extern double (*sign)(double);
		extern void (*clampAngleDeg)(double&);
	};
	namespace i
	{
		bool isPowerOf2(unsigned int x);
	};

	template <typename T>
	void linearInterpolationV(unsigned int length, const T start[], const T end[], T result[], float alpha)
	{
		float omalpha = 1 - alpha;
		for (int i=0; i!=length; i++)
		{
			result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
		}
	}
	// if you have a known at compile time length this is better
	template<typename T, unsigned int length>
	inline void linearInterpolationV(const T start[], const T end[], T result[], float alpha)
	{
		float omalpha = 1 - alpha;
		for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
		{
			result[i] = static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha;
		}
	}
	// use this with integers numbers
	template<typename T, unsigned int length>
	inline void linearInterpolationRoundV(const T start[], const T end[], T result[], float alpha)
	{
		float omalpha = 1 - alpha;
		for (int i=0; i!=length; i++) // hopefully this loop will be unrolled
		{
			result[i] = ROUND(static_cast<float>(start[i])*alpha + static_cast<float>(end[i])*omalpha);
		}
	}

	uint32_t roundToNextPow2(uint32_t v);
};
// #endif // MATHTOOLS_H