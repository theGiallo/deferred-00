/* 
 * File:   Matrix4.h
 * Author: thegiallo
 *
 * Created on 29 gennaio 2011, 15.44
 */

#ifndef MATRIX4_H
#define	MATRIX4_H

#include "Vec3.h"

#include <iostream>
#include <vector>

#ifndef Uint8
#define Uint8 uint8_t
#endif

namespace tgmath
{
	namespace f
	{

class Frustum;

/**
 * 4x4 float matrix row-dominant
 *
 * Be aware that you have to use
 *
 * layout(row_major) uniform; // default is now row_major
 *
 * in all the shaders, otherwise you have to transpose the matrix before passing to OGL
 */
class Matrix4
{
protected:
//	#pragma pack(push, 1)
	struct matrix_st
	{
		float*rows[4];
		float cells[16];
	};
//	#pragma pack(pop)
	float** matrix;
	struct matrix_st mem;
public:
	Matrix4(const Matrix4& m);
	Matrix4(void);
	Matrix4(float a00, float a01, float a02, float a03,
	        float a10, float a11, float a12, float a13,
	        float a20, float a21, float a22, float a23,
	        float a30, float a31, float a32, float a33 );
	~Matrix4(void);
	/**
	 * Returns a copy of the matrix, i.e. a memory contiguous bidimensional
	 * array. It has to be deallocated with "delete".
	 *
	 * Example:
	 *
	 * Matrix4 M();
	 * float** m = M.getMatrix();
	 * // <some operation on m>
	 * delete m;
	 */
	float** getMatrix(void) const;
	const float** getMatrix_const(void) const;
	const float* getArray() const;
	void set(Uint8 i, Uint8 j, float val);
	float get(Uint8 i, Uint8 j);
	void set(float a00, float a01, float a02, float a03,
	         float a10, float a11, float a12, float a13,
	         float a20, float a21, float a22, float a23,
	         float a30, float a31, float a32, float a33 );
	Matrix4& operator = (const Matrix4& m);
	const Matrix4 operator *(const Matrix4& m) const;
	void mult(const Matrix4& m);

	/**
	 * Multiply the matrix by the vector as (x,y,z,1) [as a point].
	 * Before returning w of res is made 0 dividing res by w.
	 * @param v the vector to be multiplied
	 *
	 * @return a Vec3 resulting from the multiplication
	 */
	const Vec3 operator * (const Vec3& v) const;
	/**
	 * Multiply the matrix by the vector as (x,y,z,0) [as a vector].
	 * @param v the vector to be multiplied
	 *
	 * @return a Vec3 resulting from the multiplication
	 */
	const Vec3 operator ^ (const Vec3& v) const;

	/**
	 * Transpone the matrix
	 *
	 * @return transpose of this matrix
	 */
	const Matrix4 operator *(void) const;

	std::ostream& print(std::ostream& os) const;
	static const Matrix4 I;

	/**
	 * Builds a transform matrix given translation, scale and rotation values.
	 * @param tr translation
	 * @param rot rotation
	 * @param scale scale
	 *
	 * @return transform matrix
	 */
	static Matrix4 transformMatrix(const Vec3& tr, const Vec3& rot, const Vec3& scale);
	/**
	 * Builds a transform matrix given translation, scale and rotation values.
	 * @param tr translation
	 * @param rot rotation in degrees
	 * @param scale scale
	 * @param res where the output matrix will be built
	 *
	 * @return reference to the matrix res
	 */
	static Matrix4& transformMatrix(const Vec3& tr, const Vec3& rot, const Vec3& scale, Matrix4& res);


	/**
	 * Builds a transform matrix given translation
	 * @param tr translation
	 *
	 * @return transform matrix
	 */
	static Matrix4 translationMatrix(const Vec3& tr);
	/**
	 * Builds a transform matrix given translation
	 * @param tr translation
	 * @param res where the output matrix will be built
	 *
	 * @return reference to the matrix res
	 */
	static Matrix4& translationMatrix(const Vec3& tr, Matrix4& res);

	/**
	 * Builds a rotation matrix given translation
	 * @param rot rotation in degrees
	 * @param res where the output matrix will be built
	 *
	 * @return reference to the matrix res
	 */
	static Matrix4& rotationMatrix(const Vec3& rot, Matrix4& res);
	/**
	 * Builds a scale matrix given translation
	 * @param scale rotation
	 * @param res where the output matrix will be built
	 *
	 * @return reference to the matrix res
	 */
	static Matrix4& scaleMatrix(const Vec3& scale, Matrix4& res);

#ifdef FRUSTUM
	/**
	 * Builds a perspective projection matrix based on the given frustum and vanishing_point
	 * @param frustum the frustum the projection is based on
	 * @param vanishing_point the position in the image of the vanishing point. In percentage from 0 to 1
	 * @param res where the output matrix will be built
	 *
	 * @return reference to res matrix
	 */
	static Matrix4& perspectiveMatrix(const Frustum& frustum, const Vec2& vanishing_point, float width, float height, Matrix4& res);
#endif

	/**
	 * Builds a view matrix based on the input data
	 * @param look the look vector of the camera
	 * @param up the up vector of the camera
	 * @param pos the position of the camera
	 * @param res where the output matrix will be built
	 *
	 * @return reference to res matrix
	 */
	static Matrix4& viewMatrix(const Vec3& look, const Vec3& up, const Vec3& pos, Matrix4& res);

};

/**
 * stack to be used like opengl one
 **/
class Matrix4Stack
{
protected:
	std::vector<Matrix4> stack;
public:
	static Matrix4 dumb; // used to initialize the current reference
	Matrix4Stack();
	Matrix4Stack(Matrix4& m);
	void pop();
	void push();
	Matrix4* current;
};

}// namespace
}// namespace
#endif	/* MATRIX4_H */

