#pragma once

#include <cstdint>

namespace tgmath
{

	// NOTE: uint64_t seeds[2]; The state must be seeded so that it is not everywhere zero
	uint64_t xorshift128plus(uint64_t seeds[2]);

	float normalAVGRNDXorShiftPlus(uint64_t seeds[2], int rng_count);
}