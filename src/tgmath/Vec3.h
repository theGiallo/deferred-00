/* 
 * File:	Vec3.h
 * Author: thegiallo
 *
 * Created on 28 gennaio 2011, 0.05
 */

// #ifndef VEC3_H
// #define	VEC3_H
#pragma once

#include <cstdint>

namespace tgmath
{
	namespace i
	{
		class Vec3;
	};
	namespace f
	{
		class Vec3;
		class Quaternion;

		class Vec3
		{
		public:
				struct{float x,y,z;};

				static const uint8_t X_COMPONENT = 0;
				static const uint8_t Y_COMPONENT = 1;
				static const uint8_t Z_COMPONENT = 2;

				Vec3();
				Vec3(float x, float y, float z);
				Vec3(const tgmath::i::Vec3& v);

				operator tgmath::i::Vec3();

				float getModule(void) const;
				float getSin(const Vec3& v) const;
				float getCos(const Vec3& v) const;
				void normalize(void);
				Vec3 getNormalized(void) const;
				void add(const Vec3& v);
				const Vec3 operator +(const Vec3& v) const;
				Vec3& operator +=(const Vec3& v);
				void sub(const Vec3& v);
				const Vec3 operator -(void) const;
				const Vec3 operator -(const Vec3& v) const;
				Vec3& operator -=(const Vec3& v);
				void add(uint8_t i, float f); /// adds as if it was an array {x,y,z}
				void add(float k);
				const Vec3 operator +(float k) const;
				Vec3& operator +=(float k);
				void sub(float k);
				const Vec3 operator -(float k) const;
				Vec3& operator -=(float k);
				void mult(uint8_t i, float f); /// multiplies as if it was an array {x,y,z}
				void mult(float k);
				const Vec3 operator *(float k) const;
				Vec3& operator *=(float k);
				void div(uint8_t i, float f); /// divides as if it was an array {x,y,z}
				void div(float k);
				const Vec3 operator/(float k) const;
				Vec3& operator/=(float k);
				float getScalar(const Vec3& v) const;
				const float operator *(const Vec3& v) const;
				Vec3 getVectorial(const Vec3& v) const;
				const Vec3 operator ^(const Vec3& v) const;
				Vec3& operator ^=(const Vec3& v);
				bool isEqual(const Vec3& v);
				bool operator ==(const Vec3& v);
				bool operator !=(const Vec3& v);
				void clone(const Vec3& v);
				Vec3 getClone(void) const;
				Vec3& operator =(const Vec3& v);
				float get(uint8_t i); /// returns as if it was an array {x,y,z}
				float getX(void) const;
				float getY(void) const;
				float getZ(void) const;
				float const & operator [](unsigned int i) const;
				float & operator [](unsigned int i);
				operator const float*() const;
				operator float*();
				void set(uint8_t i, float f); /// sets as if it was an array {x,y,z}
				void setX(float x);
				void setY(float y);
				void setZ(float z);
				void print(void) const;
				void rotate(const Quaternion& q, const Vec3& center = Vec3());
				Vec3 getRotated(const Quaternion& q, const Vec3& center = Vec3()) const;
				void scale(const Vec3& s, const Vec3& center = Vec3());
				Vec3 getScaled(const Vec3& s, const Vec3& center = Vec3()) const;
		};

	}; // namespace
	namespace i
	{
		class Vec3
		{
		public:
				struct{int x,y,z;};

				static const uint8_t X_COMPONENT = 0;
				static const uint8_t Y_COMPONENT = 1;
				static const uint8_t Z_COMPONENT = 2;
				
				Vec3();
				Vec3(int x, int y, int z);
				Vec3(int* xyz);
				Vec3(const tgmath::f::Vec3& v);
				
				operator tgmath::f::Vec3();
				const Vec3 operator +(const Vec3& v) const;
				Vec3& operator +=(const Vec3& v);
				const Vec3 operator -(const Vec3& v) const;
				Vec3& operator -=(const Vec3& v);
				bool operator ==(const Vec3& v);
				bool operator !=(const Vec3& v);
				
				void print(void) const;
				
		};

	}; // namespace
}; // namespace

#include "Quaternion.h"


//#endif  /* VEC3_H */