/* 
 * File:   Vec2.h
 * Author: thegiallo
 *
 * Created on 28 gennaio 2011, 1.33
 */

// #ifndef VEC2_H
// #define	VEC2_H
#pragma once

#include <cstdint>
#include <ostream>

namespace tgmath
{
	namespace i
	{
		class Vec2;
	};
	namespace f
	{
		class Vec3;

		class Vec2
		{
		public:
			struct{float x,y;};

			static const uint8_t X_COMPONENT = 0;
			static const uint8_t Y_COMPONENT = 1;

			Vec2();
			Vec2(float x, float y);
			Vec2(const tgmath::i::Vec2& v);
			float getModule(void) const;
			float getSqModule(void) const;
			float getSin(const Vec2& v) const;
			float getCos(const Vec2& v) const;
			float getAngleRad(const Vec2& v) const;
			float getAngleDeg(const Vec2& v) const;
			float getAsAngleDeg() const;
			float getAsAngleRad() const;
			void normalize(void);
			Vec2 getNormalized(void) const;
			void add(const Vec2& v);
			const Vec2 operator +(const Vec2& v) const;
			Vec2& operator +=(const Vec2& v);
			void sub(Vec2 v);
			const Vec2 operator -(void) const;
			const Vec2 operator -(const Vec2& v) const;
			Vec2& operator -=(const Vec2& v);
			void add(uint8_t i, float f); /// adds as if it was an array {x,y}
			void add(float k);
			const Vec2 operator +(float k) const;
			Vec2& operator +=(float k);
			void sub(float k);
			const Vec2 operator -(float k) const;
			Vec2& operator -=(float k);
			void mult(uint8_t i, float f); /// multiply as if it was an array {x,y}
			void mult(float k);
			const Vec2 operator *(float k) const;
			Vec2& operator *=(float k);
			void div(uint8_t i, float f); /// divides as if it was an array {x,y}
			void div(float k);
			const Vec2 operator/(float k) const;
			Vec2& operator/=(float k);
			float scalar(const Vec2& v) const;
			const float operator *(const Vec2& v) const;
		//	Vec2 getVectorial(Vec2 v);
			Vec3 operator ^(const Vec2& v) const;
		//	Vec2 operator ^=(Vec2 v);
			bool isEqual(const Vec2& v) const;
			bool isEqual(const Vec2& v, float epsilon) const;
			bool operator ==(const Vec2& v) const;
			bool operator !=(const Vec2& v) const;
			void clone(const Vec2& v);
			Vec2 getClone(void) const;
			Vec2& operator =(const Vec2& v);
			float get(uint8_t i) const; /// returns as if it was an array {x,y}
			float getX(void) const;
			float getY(void) const;
			float const & operator [](unsigned int i) const;
			float & operator [](unsigned int i);
			operator const float*() const;
			operator float*();
			void set(uint8_t i, float f); /// sets as if it was an array {x,y}
			void setX(float x);
			void setY(float y);
			void setXY(float x, float y);
			void rotateRad(float rot);
			void rotateDeg(float rot);
			Vec2 getRotatedRad(float rot) const;
			Vec2 getRotatedDeg(float rot) const;
			void rotateRad(float rot, const Vec2& center);
			void rotateDeg(float rot, const Vec2& center);
			Vec2 getRotatedRad(float rot, const Vec2& center) const;
			Vec2 getRotatedDeg(float rot, const Vec2& center) const;
			void print(std::ostream& os) const;
		}; // class

		/**
		 * the up vector (0,1) is considered the zero rotation
		 * so x and y are swapped with respect to a normal rotation
		 **/
		void buildMatrixRotFromUpVector(const Vec2& up, float* matrix);

	}; // namespace
	namespace i
	{
		class Vec2
		{
		public:
			struct{int x,y;};

			static const uint8_t X_COMPONENT = 0;
			static const uint8_t Y_COMPONENT = 1;

			Vec2();
			Vec2(int x, int y);
			Vec2(const tgmath::f::Vec2& v);

			operator tgmath::f::Vec2();

			float getModule(void) const;
			float getSqModule(void) const;
			float getSin(const Vec2& v) const;
			float getCos(const Vec2& v) const;
			float getAngleRad(const Vec2& v) const;
			float getAngleDeg(const Vec2& v) const;
			void add(const Vec2& v);
			const Vec2 operator +(const Vec2& v) const;
			Vec2& operator +=(const Vec2& v);
			void sub(Vec2 v);
			const Vec2 operator -(void) const;
			const Vec2 operator -(const Vec2& v) const;
			Vec2& operator -=(const Vec2& v);
			void add(uint8_t i, int f); /// adds as if it was an array {x,y}
			void add(int k);
			const Vec2 operator +(int k) const;
			Vec2& operator +=(int k);
			void sub(int k);
			const Vec2 operator -(int k) const;
			Vec2& operator -=(int k);
			void mult(uint8_t i, float f); /// multiply as if it was an array {x,y}
			void mult(float k);
			const Vec2 operator *(float k) const;
			Vec2& operator *=(float k);
			void div(uint8_t i, float f); /// divides as if it was an array {x,y}
			void div(float k);
			const Vec2 operator/(float k) const;
			Vec2& operator/=(float k);
			void mult(uint8_t i, int f); /// multiply as if it was an array {x,y}
			void mult(int k);
			const Vec2 operator *(int k) const;
			Vec2& operator *=(int k);
			void div(uint8_t i, int f); /// divides as if it was an array {x,y}
			void div(int k);
			const Vec2 operator/(int k) const;
			Vec2& operator/=(int k);
			bool isEqual(const Vec2& v) const;
			bool operator ==(const Vec2& v) const;
			bool operator !=(const Vec2& v) const;
			void clone(const Vec2& v);
			Vec2 getClone(void) const;
			Vec2& operator =(const Vec2& v);
			int get(uint8_t i) const; /// returns as if it was an array {x,y}
			int getX(void) const;
			int getY(void) const;
			int const & operator [](unsigned int i) const;
			int & operator [](unsigned int i);
			operator const int*() const;
			operator int*();
			void set(uint8_t i, int f); /// sets as if it was an array {x,y}
			void setX(int x);
			void setY(int y);
			void setXY(int x, int y);
			void rotateRad(float rot);
			void rotateDeg(float rot);
			Vec2 getRotatedRad(float rot) const;
			Vec2 getRotatedDeg(float rot) const;
			void rotateRad(float rot, const Vec2& center);
			void rotateDeg(float rot, const Vec2& center);
			Vec2 getRotatedRad(float rot, const Vec2& center) const;
			Vec2 getRotatedDeg(float rot, const Vec2& center) const;
			void print(std::ostream& os) const;
		}; // class
	}; // namespace
}; // namespace

#include "Vec3.h"

// #endif  /* VEC2_H */