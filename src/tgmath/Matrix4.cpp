#include "Matrix4.h"
#include "MathConsts.h"
#include "MathTools.h"

#ifdef FRUSTUM
#include "../graphics/Frustum.h"
#endif

#include <cstring>
#include <cstdint>
#include <iostream>
#include <cmath>

namespace tgmath
{
	namespace f
	{

const Matrix4 Matrix4::I(	1,0,0,0,
							0,1,0,0,
							0,0,1,0,
							0,0,0,1 );

Matrix4::Matrix4(const Matrix4& m) : Matrix4()
{
	operator = (m);
}
Matrix4::Matrix4()
{
	matrix = &mem.rows[0];
//	(float**)malloc(sizeof(float)*16+sizeof(float*)*4);
	if (matrix==NULL)
	{
		std::cerr<<"Error with malloc at line "<<__LINE__<<" of file "<<__FILE__<<std::endl;
		exit(1);
	}
	//float *data = (float*)&matrix[4];
	float *data = &mem.cells[0];
#ifdef DEBUG_MATRIX4
	std::cout<<"\t&mem     = "<<&mem<<std::endl;
	std::cout<<"\tmem.rows = "<<mem.rows<<std::endl;
	std::cout<<"\tmem.cells= "<<mem.cells<<std::endl;
	std::cout<<"\tmatrix   = "<<matrix<<std::endl;
	std::cout<<"\tdata     = "<<data<<std::endl;
	std::cout<<"\t&cells[16]="<<&mem.cells[16]<<std::endl;
#endif
	for (int i=0; i<4; i++)
	{
		matrix[i]=&data[4*i];
#ifdef DEBUG_MATRIX4
		std::cout<<"\t&matrix["<<i<<"]="<<&matrix[i]<<std::endl;
		std::cout<<"\t&rows["<<i<<"]  ="<<&mem.rows[i]<<std::endl;
		std::cout<<"\trows["<<i<<"]   ="<<mem.rows[i]<<std::endl;
//		std::cout<<"matrix["<<i<<"]="<<matrix[i]<<std::endl;
#endif
#ifdef NO_MEMCPY
		for (int j=0 ; j<4 ; j++)
		{
			matrix[i][j] = 0;
		}
#else
		float z[4] = {0,0,0,0};
		memcpy(matrix[i],&z[0],sizeof(float)*4);
#endif
	}
#ifdef DEBUG_MATRIX4
	std::cout<<"\taddress free after matrix = "<<&matrix[3][4]<<std::endl;
#endif
}
Matrix4::Matrix4(float a00, float a01, float a02, float a03,
                 float a10, float a11, float a12, float a13,
                 float a20, float a21, float a22, float a23,
                 float a30, float a31, float a32, float a33 ):Matrix4()
{
	matrix[0][0] = a00;
	matrix[0][1] = a01;
	matrix[0][2] = a02;
	matrix[0][3] = a03;

	matrix[1][0] = a10;
	matrix[1][1] = a11;
	matrix[1][2] = a12;
	matrix[1][3] = a13;

	matrix[2][0] = a20;
	matrix[2][1] = a21;
	matrix[2][2] = a22;
	matrix[2][3] = a23;

	matrix[3][0] = a30;
	matrix[3][1] = a31;
	matrix[3][2] = a32;
	matrix[3][3] = a33;
}
Matrix4::~Matrix4()
{
//	free(matrix);
#ifdef DEBUG_MATRIX4
	std::cout<<std::endl<<"\tdeleting "<<matrix<<"------------"<<std::endl;
	std::cout<<"\t&mem	 = "<<&mem<<std::endl;
	std::cout<<"\t&cells[16]="<<&mem.cells[16]<<std::endl<<std::endl;
#endif
}
/**
 * Returns a copy of the matrix, i.e. a memory contiguous bidimensional
 * array. It has to be deallocated with "delete".
 *
 * Example:
 *
 * Matrix4 M();
 * float** m = M.getMatrix();
 * // <some operation on m>
 * delete m;
 */
float** Matrix4::getMatrix(void) const
{
	float **ret;// = (float**)malloc(sizeof(float)*16+sizeof(float*)*4);
	float *data;// = (float*)&ret[4];
	struct matrix_st *m = new struct matrix_st();
	ret = &m->rows[0];
	data = &m->cells[0];
	for (int i=0; i<4; i++)
	{
		ret[i]=&data[4*i];
#ifdef NO_MEMCPY
		for (int j=0 ; j<4 ; j++)
		{
			ret[i][j] = matrix[i][j];
		}
#else
		memcpy(ret[i],matrix[i],sizeof(float)*4);
#endif
	}
	return ret;
}
const float** Matrix4::getMatrix_const(void) const
{
	return (const float**)&matrix[0];
}
const float* Matrix4::getArray() const
{
	return mem.cells;
}
void Matrix4::set(Uint8 i, Uint8 j, float val)
{
	matrix[i][j] = val;
}
float Matrix4::get(Uint8 i, Uint8 j)
{
	return matrix[i][j];
}
void Matrix4::set(float a00, float a01, float a02, float a03,
	               float a10, float a11, float a12, float a13,
	               float a20, float a21, float a22, float a23,
	               float a30, float a31, float a32, float a33 )
{
	matrix[0][0] = a00;
	matrix[0][1] = a01;
	matrix[0][2] = a02;
	matrix[0][3] = a03;

	matrix[1][0] = a10;
	matrix[1][1] = a11;
	matrix[1][2] = a12;
	matrix[1][3] = a13;

	matrix[2][0] = a20;
	matrix[2][1] = a21;
	matrix[2][2] = a22;
	matrix[2][3] = a23;

	matrix[3][0] = a30;
	matrix[3][1] = a31;
	matrix[3][2] = a32;
	matrix[3][3] = a33;
}
Matrix4& Matrix4::operator = (const Matrix4& m)
{
	if (this!=&m)
	{
#ifdef NO_MEMCPY
		for (int i=0; i<4 ; i++)
		{
			for (int j=0 ; j<4 ; j++)
			{
				matrix[i][j] = m.matrix[i][j];
			}
		}
#else
//		for (int i=0; i<4; i++)
//		{
//	   	memcpy(matrix[i],m.matrix[i],sizeof(float)*4);
//		}
		memcpy(matrix[0],m.matrix[0],sizeof(float)*4*4);
#endif
	}
	return *this;
}
const Matrix4 Matrix4::operator *(const Matrix4& m) const
{
#ifdef DEBUG_MATRIX4
	std::cout <<"\tline " << __LINE__ << " of file " << __FILE__ << std::endl;
#endif
	Matrix4 res = *this;
	res.mult(m);
	return res;
}
void Matrix4::mult(const Matrix4& m)
{
	Matrix4 old = Matrix4(*this);
	const float** m0 = old.getMatrix_const();
	const float** m1 = m.getMatrix_const();
	for (int i=0 ; i<4 ; i++)
	{
		for (int j=0; j<4 ; j++)
		{
			matrix[i][j] = m0[i][0]*m1[0][j]+
			               m0[i][1]*m1[1][j]+
			               m0[i][2]*m1[2][j]+
			               m0[i][3]*m1[3][j];
		}
	}
}

std::ostream& Matrix4::print(std::ostream& os) const
{
	return os
	    <<matrix[0][0]<<" "<<matrix[0][1]<<" "<<matrix[0][2]<<" "<<matrix[0][3]<<std::endl
	    <<matrix[1][0]<<" "<<matrix[1][1]<<" "<<matrix[1][2]<<" "<<matrix[1][3]<<std::endl
	    <<matrix[2][0]<<" "<<matrix[2][1]<<" "<<matrix[2][2]<<" "<<matrix[2][3]<<std::endl
	    <<matrix[3][0]<<" "<<matrix[3][1]<<" "<<matrix[3][2]<<" "<<matrix[3][3]<<std::endl;
}


/**
 * Multiply the matrix by the vector as (x,y,z,1) [as a point].
 * Before returning w of res is made 0 dividing res by w.
 * @param v the vector to be multiplied
 *
 * @return a Vec3 resulting from the multiplication
 */
const Vec3 Matrix4::operator * (const Vec3& v) const
{
	Vec3 res;
	for (int i=0 ; i<3 ; i++)
	{
		res[i] = matrix[i][0]*v[0]+
		         matrix[i][1]*v[1]+
		         matrix[i][2]*v[2]+
		         matrix[i][3]*1.0f;
	}
	float w = matrix[3][0]*v[0]+
	          matrix[3][1]*v[1]+
	          matrix[3][2]*v[2]+
	          matrix[3][3]*1.0f;
	return res/w;
}

/**
 * Multiply the matrix by the vector as (x,y,z,0) [as a vector].
 * @param v the vector to be multiplied
 *
 * @return a Vec3 resulting from the multiplication
 */
const Vec3 Matrix4::operator ^ (const Vec3& v) const
{
	Vec3 res;
	for (int i=0 ; i<3 ; i++)
	{
		res[i] = matrix[i][0]*v[0]+
		         matrix[i][1]*v[1]+
		         matrix[i][2]*v[2];
	}
	return res;
}

/**
 * Transposes the matrix
 *
 * @return transpose of this matrix
 */
const Matrix4 Matrix4::operator *(void) const
{
	return Matrix4(matrix[0][0], matrix[1][0],matrix[2][0],matrix[3][0],
	               matrix[0][1], matrix[1][1],matrix[2][1],matrix[3][1],
	               matrix[0][2], matrix[1][2],matrix[2][2],matrix[3][2],
	               matrix[0][3], matrix[1][3],matrix[2][3],matrix[3][3]);
}

/**
 * Builds a transform matrix given translation
 * This function is not so good due to memory alloc/free, please use Matrix4& Matrix4::translationMatrix(const Vec3& tr, Matrix4& res)
 * @param tr translation
 *
 * @return transform matrix
 */


// STATIC //////////////////////////////////////////////////////////////////////

/**
 * Builds a transform matrix given translation, scale and rotation values.
 * @param tr translation
 * @param rot rotation
 * @param scale scale
 *
 * @return transform matrix
 */

Matrix4 Matrix4::transformMatrix(const Vec3& tr, const Vec3& rot, const Vec3& scale)
{
#ifdef DEBUG_MATRIX4
	std::cout <<"\tline " << __LINE__ << " of file " << __FILE__ << std::endl;
#endif
	Matrix4 res;
	return transformMatrix(tr,rot,scale,res);
}
/**
 * Builds a transform matrix given translation, scale and rotation values.
 * @param tr translation
 * @param rot rotation in degrees
 * @param scale scale
 * @param res matrix that will store the result
 *
 * @return reference to the matrix res
 */
Matrix4& Matrix4::transformMatrix(const Vec3& tr, const Vec3& rot, const Vec3& scale, Matrix4& res)
{
#ifdef DEBUG_MATRIX4
	std::cout <<"\tline " << __LINE__ << " of file " << __FILE__ << std::endl;
#endif
	Matrix4 S,R,T;
	Matrix4::scaleMatrix(scale,S);
	Matrix4::rotationMatrix(rot,R);
	Matrix4::translationMatrix(tr,T);
	res = T*R*S;
	return res;
}


/**
 * Builds a rotation matrix given translation
 * @param rot rotation in degrees
 * @param res where the output matrix will be built
 *
 * @return reference to the matrix res
 */
Matrix4& Matrix4::rotationMatrix(const Vec3& rot, Matrix4& res)
{
	Vec3 r(DEG2RAD(rot.x),
	       DEG2RAD(rot.y),
	       DEG2RAD(rot.z));
	Matrix4 Rx(1.0f, .0f,          .0f,            .0f,
	           .0f, std::cos(r.x), -std::sin(r.x), .0f,
	           .0f, std::sin(r.x), std::cos(r.x),  .0f,
	           .0f, .0f,           .0f,            1.0f
	          );
	Matrix4 Ry(std::cos(r.y),  .0f,  std::sin(r.y), .0f,
	           .0f,            1.0f, .0f,           .0f,
	           -std::sin(r.y), .0f,  std::cos(r.y), .0f,
	           .0f,            .0f,  .0f,           1.0f
	          );
	Matrix4 Rz(std::cos(r.z), -std::sin(r.z), .0f,  .0f,
	           std::sin(r.z), std::cos(r.z),  .0f,  .0f,
	           .0f,           .0f,            1.0f, .0f,
	           .0f,           .0f,            .0f,  1.0f
	          );
	res = Rz*Ry*Rx;
	return res;
}
/**
 * Builds a scale matrix given translation
 * @param scale rotation
 * @param res where the output matrix will be built
 *
 * @return reference to the matrix res
 */
Matrix4& Matrix4::scaleMatrix(const Vec3& scale, Matrix4& res)
{
	res.set(scale.x, .0f,     .0f,     .0f,
	        .0f,     scale.y, .0f,     .0f,
	        .0f,     .0f,     scale.z, .0f,
	        .0f,     .0f,     .0f,     1.0f
	       );
	return res;
}

Matrix4 Matrix4::translationMatrix(const Vec3& tr)
{
	std::cout <<"please use the other function  translationMatrix"<< std::endl;
#ifdef DEBUG_MATRIX4
	std::cout <<"\tline " << __LINE__ << " of file " << __FILE__ << std::endl;
#endif
	Matrix4 res;
	return translationMatrix(tr,res);
}

/**
 * Builds a transform matrix given translation
 * @param tr translation
 * @param res where the result will be stored
 *
 * @return reference to the matrix res
 */
Matrix4& Matrix4::translationMatrix(const Vec3& tr, Matrix4& res)
{
#ifdef DEBUG_MATRIX4
	std::cout <<"\tline " << __LINE__ << " of file " << __FILE__ << std::endl;
#endif
	res.set(	1.0f, .0f, .0f,   tr.x,
	        .0f,   1.0f, .0f,  tr.y,
	        .0f,   .0f,  1.0f, tr.z,
	        .0f,   .0f,  .0f,  1.0f
	       );
	return res;
}

#ifdef FRUSTUM
/**
 * Builds a perspective projection matrix based on the given frustum and vanishing_point
 * @param frustum the frustum the projection is based on
 * @param vanishing_point the position in the image of the vanishing point. In percentage from 0 to 1
 * @param width width of the near plane
 * @param height height of the near plane
 * @param res where the output matrix will be built
 *
 * @return reference to res matrix
 */
Matrix4& Matrix4::perspectiveMatrix(const Frustum& frustum, const Vec2& vanishing_point, float width, float height, Matrix4& res)
{
	float left = -vanishing_point.x*width;
	float right = (1-vanishing_point.x)*width;
	float bottom = -vanishing_point.y*height;
	float top = (1-vanishing_point.y)*height;

	/**
	 * gml style
	 */
	res.set(0,0, (2 * frustum.near) / (right - left));
	res.set(1,1, (2 * frustum.near) / (top - bottom));
	res.set(2,2, -(frustum.far + frustum.near) / (frustum.far - frustum.near));
	res.set(3,2, -1);
	res.set(2,3, -(2 * frustum.far * frustum.near) / (frustum.far - frustum.near));
	res.set(0,1, 0.0f);
	res.set(0,2, 0.0f);
	res.set(0,3, 0.0f);
	res.set(1,0, 0.0f);
	res.set(1,2, 0.0f);
	res.set(1,3, 0.0f);
	res.set(2,0, 0.0f);
	res.set(2,1, 0.0f);
	res.set(3,0, 0.0f);
	res.set(3,1, 0.0f);
	res.set(3,3, 0.0f);

	return res;
}
#endif

/**
 * Builds a view matrix based on the input data
 * @param look the look vector of the camera
 * @param up the up vector of the camera
 * @param pos the position of the camera
 * @param res where the output matrix will be built
 *
 * @return reference to res matrix
 */
Matrix4& Matrix4::viewMatrix(const Vec3& look, const Vec3& up, const Vec3& pos, Matrix4& res)
{
	Vec3 n = look.getNormalized();
	Vec3 u = up^look; u.normalize();
	Vec3 v = n^u; v.normalize();

	/**
	 * gml style
	 */
	res.set(0,0, -u.x);
	res.set(0,1, -u.y);
	res.set(0,2, -u.z);

	res.set(1,0, v.x);
	res.set(1,1, v.y);
	res.set(1,2, v.z);

	res.set(2,0, -n.x);
	res.set(2,1, -n.y);
	res.set(2,2, -n.z);

	res.set(0,3, u*pos);
	res.set(1,3, -v*pos);
	res.set(2,3, n*pos);

	res.set(3,0, .0f);
	res.set(3,0, .0f);
	res.set(3,0, .0f);
	res.set(3,3, 1.0f);

	return res;
}

// Matrix4Stack
//---
Matrix4 Matrix4Stack::dumb = Matrix4();
Matrix4Stack::Matrix4Stack() :
                           current(nullptr)
{
	stack.push_back(Matrix4::I);
	current = &stack.back();
}
Matrix4Stack::Matrix4Stack(Matrix4& m) :
                           current(&m)
{
	stack.push_back(m);
	current = &stack.back();
	current->print(std::cout);
}

void Matrix4Stack::pop()
{
	stack.pop_back();
	current = &stack.back();
}

void Matrix4Stack::push()
{
	stack.push_back(stack.back());
	current = &stack.back();
}

}// namespace
}// namespace