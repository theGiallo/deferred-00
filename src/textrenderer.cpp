#include "textrenderer.h"
#include "environment.h"
#include "dbg.h"
#include <iostream>

// Static variables
//---
Font Font::debug_info_font;

// const Uint8 CENTERED_W = 0x1<<0;
// const Uint8 CENTERED_H = 0x1<<1;
// const Uint8 CENTERED_BOTH = CENTERED_W | CENTERED_H;

// Methods
//---
Font::Font() : font(nullptr),
			   path(""),
			   ptsize(-1)
{}
Font::Font(const Font& f)
			 : font(f.font),
			   path(f.path),
			   ptsize(f.ptsize)
{}
Font::Font(Font&& f)
			 : font(f.font),
			   path(std::move(f.path)),
			   ptsize(f.ptsize)
{
	f.font = nullptr;
	f.path = "";
	f.ptsize = -1;
}
Font::Font(std::string path, int ptsize)
			 : font(nullptr),
			   path(path),
			   ptsize(ptsize)
{}
Font& Font::operator =(Font&& f)
{
	font = f.font;
	path = std::move(f.path);
	ptsize = f.ptsize;

	f.font = nullptr;
	f.ptsize = -1;

	return *this;
}
Font& Font::operator =(const Font& f)
{
	font = f.font;
	path = f.path;
	ptsize = f.ptsize;

	return *this;
}
bool Font::isOK() const
{
	return font!=nullptr;
}
int Font::getSize() const
{
	return ptsize;
}
const TTF_Font* Font::getFont() const
{
	return font;
}
TTF_Font* Font::getFont()
{
	return font;
}
const std::string& Font::getPath() const
{
	return path;
}
int Font::getMaxHeight() const
{
	return TTF_FontHeight(font);
}

// Static
//---

// Attributes
//---
std::map<std::string, std::map<int, Font>> Font::loaded_fonts;

// Methods
//---
void
Font::renderText(const std::string& text, const Font& font, Uint8 mask_centered, const ColorRGBA& color, const SDL_Rect& area, SDL_Rect * text_area)
{
	renderText(text, font, mask_centered, SDL_Color{color.r, color.g, color.b}, color.a, area, text_area);
}
void
Font::renderText(const std::string& text, const Font& font, Uint8 mask_centered, const SDL_Color& color, Uint8 alpha, SDL_Rect area, SDL_Rect * text_area)
{
	
	int ww, wh;
	SDL_GetWindowSize(Environment::window, &ww, &wh);
	int text_w = 0,
	    text_h = 0,
	    text_x = area.x,
	    text_y = wh - area.y;
	if (area.w==0 && area.h==0)
	{
		area.x =
		area.y = 0;
		area.w = ww;
		area.h = wh;
	}

	Font font_nonconst = font;

	if (TTF_SizeText(font_nonconst.getFont(), text.c_str(), &text_w, &text_h) == -1)
	{
		// Error...
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Error determining TTF test size at line %d of file %s",__LINE__,__FILE__);
	}

	if (mask_centered & CENTERED_W)
	{
		text_x = area.x + (area.w - text_w) / 2;
	}

	if (mask_centered & CENTERED_H)
	{
		text_y = wh - (area.y + (area.h - text_h) / 2);
	}
	renderText(text, font, text_x, text_y, color, alpha);
	if (text_area)
	{
		text_area->x = text_x;
		text_area->y = wh - text_y;
		text_area->w = text_w;
		text_area->h = text_h;
	}
}
void Font::renderText(const std::string& text, const Font& font, int x, int y, const ColorRGBA& color)
{
	renderText(text, font, x, y, {color.r, color.g, color.b}, color.a);
}
void Font::renderText(const std::string& text, const Font& font, int x, int y, const SDL_Color& color, Uint8 alpha)
{
	// getFont(font);
	if (!font.isOK())
	{
		std::cerr<<"font provided is not OK"<<std::endl;
		return;
	}

	int ret;
	SDL_Surface *text_surface;
	SDL_Texture *text_tex;
	if(!(text_surface=TTF_RenderUTF8_Blended(font.font, text.c_str(), color)))
	{
		// handle error here, perhaps print TTF_GetError at least
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Blended generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
	} else
	{

		SDL_Rect tex_size = {0,0,0,0};
		SDL_Rect tex_dest;

		tex_size.w = text_surface->w;
		tex_size.h = text_surface->h;
		tex_dest.w = tex_size.w;
		tex_dest.h = tex_size.h;

		{
			int ww, wh;
			SDL_GetWindowSize(  Environment::window, // SDL_Window* window: the window to query
			                    &ww,	// int*		w: gets the width of the window
			                    &wh);   // int*		h: gets the height of the window

			tex_dest.x = x;
			tex_dest.y = wh - y;
		}

		#if USE_SDL_RENDERER
			text_tex = SDL_CreateTextureFromSurface(Environment::renderer, text_surface);
			if (!text_tex)
			{
				const char *error = SDL_GetError();
				if (*error != '\0')
				{
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_CreateTextureFromSurface. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
					SDL_ClearError();
				}
			}
			SDL_FreeSurface(text_surface);

			if (alpha!=255)
			{
				SDL_SetTextureAlphaMod(text_tex, alpha);
			}

			ret =
			SDL_RenderCopy( Environment::renderer,	// SDL_Renderer* renderer: the renderer to affect
			                text_tex,	// SDL_Texture* texture: the source texture
			                &tex_size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
			                &tex_dest);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
			if (ret != 0)
			{
				const char *error = SDL_GetError();
				if (*error != '\0')
				{
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
					SDL_ClearError();
				}
			}

			// Destroy the texture
			SDL_DestroyTexture(text_tex);
		#else
			log_err("Not using SDL renderer!");
		#endif
	}
}
SDLWrap_Texture Font::renderTextOnTexture(const std::string& text, const Font& font, SDL_Color color, int outline)
{
	// getFont(font);
	if (!font.isOK())
	{
		std::cerr<<"font provided is not OK"<<std::endl;
		return {nullptr, SDL_Rect()};
	}

	if (outline!=-1)
	{
		int old = TTF_GetFontOutline(font.font);
		TTF_SetFontOutline(font.font,outline);
		const char *error = TTF_GetError();
		if (*error != '\0')
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set font outline. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
			// TTF_ClearError();
		}
		outline = old;
	}

	SDL_Surface *text_surface;
	SDL_Texture *text_tex;
	if(!(text_surface=TTF_RenderUTF8_Blended(font.font, text.c_str(), color)))
	{
		// handle error here, perhaps print TTF_GetError at least
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "TTF_RenderText_Blended generated an error at line %d of file %s Error:%s\n",__LINE__,__FILE__,TTF_GetError());
		return {nullptr, SDL_Rect()};
	} else
	{

		SDL_Rect tex_size = {0,0,0,0};

		tex_size.w = text_surface->w;
		tex_size.h = text_surface->h;

		#if USE_SDL_RENDERER
			text_tex = SDL_CreateTextureFromSurface(Environment::renderer, text_surface);
			if (!text_tex)
			{
				const char *error = SDL_GetError();
				if (*error != '\0')
				{
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not SDL_CreateTextureFromSurface. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
					SDL_ClearError();
				}
			}
			SDL_FreeSurface(text_surface);

			if (outline!=-1)
			{
				TTF_SetFontOutline(font.font,outline);
				const char *error = TTF_GetError();
				if (*error != '\0')
				{
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not set font outline. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
					// TTF_ClearError();
				}
			}

			return {text_tex, tex_size};
		#else
			log_err("Not using SDL renderer!");
			return {};
		#endif
	}
}
Font& Font::getFont(Font& font)
{
	std::map<std::string, std::map<int, Font>>::iterator fonts = loaded_fonts.find(font.path);
	if (fonts!=loaded_fonts.end())
	{
		std::map<int, Font>::iterator f = fonts->second.find(font.ptsize);
		if (f!=fonts->second.end())
		{
			font = f->second;
			return font;
		}
		
		font.font = TTF_OpenFont(font.path.c_str(), font.ptsize);
		if (!font.font)
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Problem loading font '%s' with pt size of %d",font.path.c_str(), font.ptsize);
			return font; // does not insert an invalid font
		}
		// turn on kerning on the loaded font
		TTF_SetFontKerning(font.font, 1);

		fonts->second[font.ptsize] = font;
		return font;
	}
	
	font.font = TTF_OpenFont(font.path.c_str(), font.ptsize);
	if (!font.font)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Problem loading font '%s' with pt size of %d",font.path.c_str(), font.ptsize);
		return font; // does not insert an invalid font
	}
	// turn on kerning on the loaded font
	TTF_SetFontKerning(font.font, 1);
	
	loaded_fonts[font.path][font.ptsize] = font;
	return font;
}
Font&& Font::getFont(Font&& font)
{
	std::map<std::string, std::map<int, Font>>::iterator fonts = loaded_fonts.find(font.path);
	if (fonts!=loaded_fonts.end())
	{
		std::map<int, Font>::iterator f = fonts->second.find(font.ptsize);
		if (f!=fonts->second.end())
		{
			font = f->second;
			return std::move(font);
		}
		
		font.font = TTF_OpenFont(font.path.c_str(), font.ptsize);
		if (!font.font)
		{
			SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Problem loading font '%s' with pt size of %d",font.path.c_str(), font.ptsize);
			return std::move(font); // does not insert an invalid font
		}
		// turn on kerning on the loaded font
		TTF_SetFontKerning(font.font, 1);


		fonts->second[font.ptsize] = font;
		return std::move(font);
	}
	
	font.font = TTF_OpenFont(font.path.c_str(), font.ptsize);
	if (!font.font)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Problem loading font '%s' with pt size of %d",font.path.c_str(), font.ptsize);
		return std::move(font); // does not insert an invalid font
	}
	// turn on kerning on the loaded font
	TTF_SetFontKerning(font.font, 1);

	
	loaded_fonts[font.path][font.ptsize] = font;
	return std::move(font);
}