#pragma once

#ifdef ANDROID
	#include <SDL.h>
	#include <SDL_video.h>
#else
	#include <SDL2/SDL.h>
	#include <SDL2/SDL_video.h>
#endif
#include <SDL_ttf.h>

#include "sdlwrap.h"

#include <string>
#include <map>

class Font
{
private:
	TTF_Font *font;
	std::string path;
	int ptsize;
public:
	static Font debug_info_font;

	Font();
	Font(const Font& f);
	Font(Font&& f);
	Font(std::string path, int ptsize);

	Font& operator =(Font&& f);
	Font& operator =(const Font& f);

	bool isOK() const;

	int getSize() const;
	const std::string& getPath() const;
	const TTF_Font* getFont() const;
	TTF_Font* getFont();
	int getMaxHeight() const;

	static const Uint8 CENTERED_W = 0x1<<0;
	static const Uint8 CENTERED_H = 0x1<<1;
	static const Uint8 CENTERED_BOTH = CENTERED_W | CENTERED_H;

	static std::map<std::string, std::map<int, Font>> loaded_fonts;
	// NOTE: text_area is an out parameter and represents the area occupied by
	// the rendered text
	static void renderText(const std::string& text, const Font& font, Uint8 mask_centered, const ColorRGBA& color, const SDL_Rect& area = {}, SDL_Rect * text_area = nullptr);
	static void renderText(const std::string& text, const Font& font, Uint8 mask_centered, const SDL_Color& color={0,0,0}, Uint8 alpha=255, SDL_Rect area = {}, SDL_Rect * text_area = nullptr);
	static void renderText(const std::string& text, const Font& font, int x, int y, const SDL_Color& color={0,0,0}, Uint8 alpha=255);
	static void renderText(const std::string& text, const Font& font, int x, int y, const ColorRGBA& color);
	static SDLWrap_Texture renderTextOnTexture(const std::string& text, const Font& font, SDL_Color color={0,0,0}, int outline = -1);
	static Font& getFont(Font& font);
	static Font&& getFont(Font&& font);
};