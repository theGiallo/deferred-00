#include "common_types.h"

namespace tg
{
	// Consts for the frame independent game loop
	//---
	#if defined ANDROID
	const double MAX_FPS = 60.0;
	#else
	const double MAX_FPS = 120.0;
	#endif
	const double MIN_FPS = 16.0;
	const double EXPECTED_FPS = 60.0;
	const double MAX_UPS = 300.0;
	const double FIXED_TIMESTEP = 1.0 / MAX_UPS;
	const double FIXED_TIMESTEP_2 = FIXED_TIMESTEP * FIXED_TIMESTEP;
	const double MIN_TIME = 1.0 / MAX_FPS;
	const double MAX_TIME = 1.0 / MIN_FPS;

	#if defined ANDROID
	const int VSYNC = 1;
	#else
	const int VSYNC = 0;
	#endif

	extern Time time_accumulator,
	            prepause_time_accumulator;
	extern bool running; // NOTE: if false game loop ends
	extern bool paused; // NOTE: you should not modify paused, call pauseSwitch()
	extern bool has_to_start;

	extern float fps;
	extern float ms_per_frame;

	Time getTimeInSec();

	void initEngine();
	void pauseSwitch();
}