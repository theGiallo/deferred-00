#include "sdlwrap.h"
#include "tgmath/MathConsts.h"
#include "dbg.h"
#include <cmath>

ColorRGBA::operator SDL_Color()
{
	return {r,b,g};
}

void getRenderColor(SDL_Renderer* renderer, ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		int ret = SDL_GetRenderDrawColor(renderer,
		                                 &color.r,
		                                 &color.g,
		                                 &color.b,
		                                 &color.a);
		DCHECKSDLERROR(ret);
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void setRenderColor(SDL_Renderer* renderer, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		int ret = SDL_SetRenderDrawColor(renderer,
		                                 color.r,
		                                 color.g,
		                                 color.b,
		                                 color.a);
		DCHECKSDLERROR(ret);
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void setRenderColor(const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		setRenderColor(Environment::renderer, color);
	#else
		log_err("Not using SDL renderer!");
	#endif
}
bool renderTexturedRect(const SDLWrap_Texture& texture, const SDL_Rect& rect, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		auto t = texture;
		int ret;
		ColorRGBA orig_c, new_c;
		ret = SDL_GetTextureAlphaMod(t.texture, &orig_c.a);
		new_c.a = ((int)color.a * (int)orig_c.a) / 255;
		ret = SDL_SetTextureAlphaMod(t.texture, new_c.a);
		ret = SDL_GetTextureColorMod(t.texture, &orig_c.r, &orig_c.g, &orig_c.b);
		new_c.r = ((int)color.r * (int)orig_c.r) / 255;
		new_c.g = ((int)color.g * (int)orig_c.g) / 255;
		new_c.b = ((int)color.b * (int)orig_c.b) / 255;
		ret = SDL_SetTextureColorMod(t.texture, new_c.r, new_c.g, new_c.b);

		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
				return false;
			}
		}
		ret =
			SDL_RenderCopy( Environment::renderer,	// SDL_Renderer* renderer: the renderer to affect
							t.texture,	// SDL_Texture* texture: the source texture
							&t.size,   // const SDL_Rect* srcrect: the source SDL_Rect structure or NULL for the entire texture  
							&rect);  // const SDL_Rect* dstrect: the destination SDL_Rect structure or NULL for the entire rendering target. The texture will be stretched to fill the given rectangle.
			if (ret != 0)
			{
				const char *error = SDL_GetError();
				if (*error != '\0')
				{
					SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not rendercopy. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
					SDL_ClearError();
					return false;
				}
			}

		ret = SDL_SetTextureColorMod(t.texture, orig_c.r, orig_c.g, orig_c.b);
		ret = SDL_SetTextureAlphaMod(t.texture, orig_c.a);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
				return false;
			}
		}
		return true;
	#else
		log_err("Not using SDL renderer!");
		return false;
	#endif
}
bool renderRectBorder(const SDL_Rect& rect, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		ColorRGBA oldc;
		getRenderColor(Environment::renderer, oldc);
		setRenderColor(Environment::renderer, color);
		int ret = SDL_RenderDrawRect(Environment::renderer, &rect);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawRect. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
			setRenderColor(Environment::renderer, oldc);
			return false;
		}
		setRenderColor(Environment::renderer, oldc);
		return true;
	#else
		log_err("Not using SDL renderer!");
		return false;
	#endif
}
bool renderRectPlain(const SDL_Rect& rect, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		ColorRGBA oldc;
		getRenderColor(Environment::renderer, oldc);
		setRenderColor(Environment::renderer, color);
		int ret = SDL_RenderFillRect(Environment::renderer, &rect);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawRect. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
			setRenderColor(Environment::renderer, oldc);
			return false;
		}
		setRenderColor(Environment::renderer, oldc);
		return true;
	#else
		log_err("Not using SDL renderer!");
		return false;
	#endif
}
bool renderLine(const tgmath::i::Vec2 start, const tgmath::i::Vec2 end, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		ColorRGBA oldc;
		getRenderColor(Environment::renderer, oldc);
		setRenderColor(Environment::renderer, color);

		int ww,wh;
		SDL_GetWindowSize(Environment::window,
		                  &ww,
		                  &wh);
		// Draw a line
		//---
		int ret =
		SDL_RenderDrawLine(
	                  Environment::renderer, // SDL_Renderer* renderer: the renderer in which draw
		               start.x,               // int x1: x of the starting point
		               wh - start.y,          // int y1: y of the starting point
		               end.x,                 // int x2: x of the end point
	                  wh - end.y);           // int y2: y of the end point
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawLine. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
			setRenderColor(Environment::renderer, oldc);
			return false;
		}
		setRenderColor(Environment::renderer, oldc);
		return true;
	#else
		log_err("Not using SDL renderer!");
		return false;
	#endif
}
bool renderPoint(const tgmath::i::Vec2& p, const ColorRGBA& color)
{
	#if USE_SDL_RENDERER
		ColorRGBA oldc;
		getRenderColor(Environment::renderer, oldc);
		setRenderColor(Environment::renderer, color);

		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		
		int ret =
		SDL_RenderDrawPoint(Environment::renderer, p.x, wh - p.y);

		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Could not renderDrawLine. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
			setRenderColor(Environment::renderer, oldc);
			return false;
		}

		setRenderColor(Environment::renderer, oldc);

		return true;
	#else
		log_err("Not using SDL renderer!");
		return false;
	#endif
}
void loadTexture(const std::string & file_path, SDLWrap_Texture & texture)
{
	SDL_Surface *bitmap_surface = NULL;
	bitmap_surface = SDL_LoadBMP(file_path.c_str());
	if (!bitmap_surface)
	{
		SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "File '%s' can't be loaded!", file_path.c_str());
		return;
	}
	#if USE_SDL_RENDERER
		if (texture.texture)
		{ 
			SDL_DestroyTexture(texture.texture);
		}
		texture.texture = SDL_CreateTextureFromSurface(Environment::renderer, bitmap_surface);
		texture.size.w = bitmap_surface->w;
		texture.size.h = bitmap_surface->h;
		texture.size.x = 0;
		texture.size.y = 0;
		SDL_FreeSurface(bitmap_surface);
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void renderTexture(const SDLWrap_Texture& texture, const tgmath::i::Vec2 & pos, float rot, float scale, Uint8 flip)
{
	#if USE_SDL_RENDERER
		int ww,wh;
		SDL_GetWindowSize(Environment::window,
		                  &ww,
		                  &wh);
		SDL_Rect tex_dest = texture.size;
		tex_dest.w *= scale;
		tex_dest.h *= scale;
		tex_dest.x = pos.x-tex_dest.w/2;
		tex_dest.y = wh - (pos.y+tex_dest.h/2);

		// SDL_Point center;

		SDL_RenderCopyEx(Environment::renderer, texture.texture, &texture.size, &tex_dest, -rot, NULL, static_cast<SDL_RendererFlip>(flip));
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void renderTexture(const SDLWrap_Texture& texture, const tgmath::i::Vec2 & pos, float rot, float scale, const ColorRGBA & color, Uint8 flip)
{
	#if USE_SDL_RENDERER
		auto t = texture;
		int ret;
		ColorRGBA orig_c, new_c;
		ret = SDL_GetTextureAlphaMod(t.texture, &orig_c.a);
		new_c.a = ((int)color.a * (int)orig_c.a) / 255;
		ret = SDL_SetTextureAlphaMod(t.texture, new_c.a);
		ret = SDL_GetTextureColorMod(t.texture, &orig_c.r, &orig_c.g, &orig_c.b);
		new_c.r = ((int)color.r * (int)orig_c.r) / 255;
		new_c.g = ((int)color.g * (int)orig_c.g) / 255;
		new_c.b = ((int)color.b * (int)orig_c.b) / 255;
		ret = SDL_SetTextureColorMod(t.texture, new_c.r, new_c.g, new_c.b);

		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
				return;
			}
		}

		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		SDL_Rect tex_dest = t.size;
		tex_dest.w *= scale;
		tex_dest.h *= scale;
		tex_dest.x = pos.x-tex_dest.w/2;
		tex_dest.y = wh - (pos.y+tex_dest.h/2);

		// SDL_Point center;

		SDL_RenderCopyEx(Environment::renderer, t.texture, &t.size, &tex_dest, -rot, NULL, static_cast<SDL_RendererFlip>(flip));


		ret = SDL_SetTextureColorMod(t.texture, orig_c.r, orig_c.g, orig_c.b);
		ret = SDL_SetTextureAlphaMod(t.texture, orig_c.a);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
		}
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void
renderTextureFromAtlas(const SDLWrap_Texture& texture_atlas,
                       const tgmath::i::Vec2 & frame_size,
                       const tgmath::i::Vec2 & atlas_size_in_frames,
                       int frame_ID,
                       const tgmath::i::Vec2 & pos,
                       float rot,
                       float scale,
                       const ColorRGBA & color,
                       Uint8 flip,
                       const tgmath::i::Vec2 & center)
{
	#if USE_SDL_RENDERER
		auto t = texture_atlas;
		int ret;
		ColorRGBA orig_c, new_c;
		ret = SDL_GetTextureAlphaMod(t.texture, &orig_c.a);
		new_c.a = ((int)color.a * (int)orig_c.a) / 255;
		ret = SDL_SetTextureAlphaMod(t.texture, new_c.a);
		ret = SDL_GetTextureColorMod(t.texture, &orig_c.r, &orig_c.g, &orig_c.b);
		new_c.r = ((int)color.r * (int)orig_c.r) / 255;
		new_c.g = ((int)color.g * (int)orig_c.g) / 255;
		new_c.b = ((int)color.b * (int)orig_c.b) / 255;
		ret = SDL_SetTextureColorMod(t.texture, new_c.r, new_c.g, new_c.b);

		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
				return;
			}
		}

		int ww,wh;
		SDL_GetWindowSize(Environment::window, &ww, &wh);
		SDL_Rect tex_dest;
		tex_dest.w = frame_size.x * scale;
		tex_dest.h = frame_size.y * scale;
		tex_dest.x = pos.x-tex_dest.w/2;
		tex_dest.y = wh - (pos.y+tex_dest.h/2);
		SDL_Rect t_src_rect;
		int X = frame_ID % atlas_size_in_frames.x,
		    Y = frame_ID / atlas_size_in_frames.x;
		assert(Y < atlas_size_in_frames.y);
		assert(texture_atlas.size.w <= atlas_size_in_frames.x * frame_size.x);
		assert(texture_atlas.size.h <= atlas_size_in_frames.y * frame_size.y);
		t_src_rect.x = X * frame_size.x;
		t_src_rect.y = Y * frame_size.y;
		t_src_rect.w = frame_size.x;
		t_src_rect.h = frame_size.y;

		SDL_Point sdl_center;
		sdl_center.x = tex_dest.w/2 + scale*center.x;
		sdl_center.y = tex_dest.h/2 + scale*center.y;
		tex_dest.x -= scale*center.x;
		tex_dest.y -= scale*center.y;

		SDL_RenderCopyEx(Environment::renderer, t.texture, &t_src_rect, &tex_dest, -rot, &sdl_center, static_cast<SDL_RendererFlip>(flip));


		ret = SDL_SetTextureColorMod(t.texture, orig_c.r, orig_c.g, orig_c.b);
		ret = SDL_SetTextureAlphaMod(t.texture, orig_c.a);
		if (ret != 0)
		{
			const char *error = SDL_GetError();
			if (*error != '\0')
			{
				SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Some problem with Texture Mod. SDL Error: %s at line #%d of file %s/n",error,__LINE__,__FILE__);
				SDL_ClearError();
			}
		}
	#else
		log_err("Not using SDL renderer!");
	#endif
}
void renderCircle(const tgmath::i::Vec2 & center, float radius, const ColorRGBA & color, int sides)
{
	assert(sides >= 0);

	if (sides == 0)
	{
		sides = tgmath::f::_2PI*radius / 2;
	}

	float d_a = tgmath::f::_2PI/sides,
	      angle = d_a;

	tgmath::i::Vec2 start, end;
	end.x = radius;
	end.y = 0.0f;
	end = end + center;
	for (int i=0; i!=sides; i++)
	{
		start = end;
		end.x = cos(angle) * radius;
		end.y = sin(angle) * radius;
		end = end + center;
		angle += d_a;
		renderLine(start, end, color);
	}
}

SDL_Rect SDLRect(int center_x, int center_y, int width, int height)
{
	int ww,wh;
	SDL_GetWindowSize(Environment::window, &ww, &wh);
	return SDL_Rect{center_x-width/2, wh - center_y - height/2, width, height};
}