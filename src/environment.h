#pragma once

#ifdef ANDROID
	#include <SDL.h>
#else
	#include <SDL2/SDL.h>
#endif

#include <vector>
#include <map>
#include "tgmath/Vec2.h"

#define MAX_CONTROLLERS 8

namespace Environment
{
	extern SDL_Window* window;
	extern SDL_GLContext glcontext;

	extern float zoom;

	extern std::vector<SDL_GameController *> game_controllers;
	extern std::map<int,int> gc_which; // NOTE: gc_which[joystick_instanceID] = joystick_number;
	extern tgmath::i::Vec2 left_stick[MAX_CONTROLLERS],
	                       right_stick[MAX_CONTROLLERS];
	extern int left_trigger[MAX_CONTROLLERS],
	           right_trigger[MAX_CONTROLLERS];

	void init();
	void update();

	int gameControllerIDFromJoyopadID(int joypad_ID);
};