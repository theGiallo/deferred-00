#pragma once

#include "common_types.h"
#include "tgmath/Vec2.h"
#include "sdlwrap.h"
namespace tg
{
	class PerfViz
	{
		// NOTE(theGiallo): you have to set the desired _buffer_size before and
		// to call init(), that will allocate the _buffer. Alternatively you can
		// allocate the memory or assign _buffer what you want.
		// The destructor will delete the memory if _owns_buffer (default false).
		// init will set _owns_buffer to true when allocating. If you want you can
		// change it after.
		// IMPORTANT for performance _buffer_size is restricted to a power of 2,
		// if not it will not correctly work. init will automatically increase it
		// up to the next power of 2.
	public:
		Time * _buffer;
		int _buffer_size;
		int _tail_id;
		bool _owns_buffer;

		int width, height;
		tgmath::i::Vec2 center;
		int samples_visualized;
		Time max_value;
		ColorRGBA graph_color;
		ColorRGBA border_color;

		bool rendered;

		enum Style
		{
			VLINES,
			AREA,
			LINESTRIP,
		};
		Style style;

		PerfViz();
		~PerfViz();
		void init();
		void reset();

		void render();
		void addSample(Time sample);

		void centerHorizontally();
		void centerVertically();
		void distFromBottom(int dist);
		void distFromTop(int dist);
		void distFromLeft(int dist);
		void distFromRight(int dist);
	};
}